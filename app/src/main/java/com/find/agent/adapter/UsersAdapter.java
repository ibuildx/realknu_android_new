package com.find.agent.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.backendless.BackendlessUser;
import com.find.agent.R;
import com.find.agent.activity.ProfileDetialActivity;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by iBuildX on 1/12/2017.
 */

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.MyViewHolder>{

    private ArrayList<BackendlessUser> dataSet;
    Context context;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewUserName;
        TextView textViewUserNumber;
        CircleImageView imageViewUserImage;
        ImageView imageViewUserNotification;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewUserName = (TextView) itemView.findViewById(R.id.list_item_user_name);
            this.textViewUserNumber = (TextView) itemView.findViewById(R.id.list_item_user_number);
            this.imageViewUserImage = (CircleImageView) itemView.findViewById(R.id.list_item_user_imageview);
            this.imageViewUserNotification = (ImageView) itemView.findViewById(R.id.list_item_user_notification_imageview);
            this.cardView = (CardView) itemView.findViewById(R.id.list_item_user_card_view);
        }
    }

    public UsersAdapter(Context _context, ArrayList<BackendlessUser> data) {
        this.context = _context;
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_activity_user, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView textViewName = holder.textViewUserName;
        TextView textViewVersion = holder.textViewUserNumber;
        ImageView imageView = holder.imageViewUserImage;

        textViewName.setText(dataSet.get(listPosition).getProperty("name").toString());
        textViewVersion.setText(dataSet.get(listPosition).getEmail());
        imageView.setImageResource(R.drawable.makeup);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, ProfileDetialActivity.class);
                intent.putExtra("firstName", dataSet.get(listPosition).getProperty("firstName").toString()+" "+dataSet.get(listPosition).getProperty("LastName").toString());
                intent.putExtra("email", dataSet.get(listPosition).getEmail());
                intent.putExtra("phone", dataSet.get(listPosition).getProperty("phone").toString());
                context.startActivity(intent);
            }
        });

        if((listPosition % 2 == 0)){
            holder.cardView.setCardBackgroundColor(Color.WHITE);
        }else{
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.material_grey_200));
        }
    }
    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}
