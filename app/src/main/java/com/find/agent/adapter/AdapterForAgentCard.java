package com.find.agent.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.find.agent.R;
import com.find.agent.dialog.MarkerDialogPopUpActivity;
import com.find.agent.fonts.CustomFontButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by leaditteam on 31.05.17.
 */

public class AdapterForAgentCard extends
        RecyclerView.Adapter<AdapterForAgentCard.MyViewHolder> {
    
    private List<BackendlessUser> mResponse;
    private Context mContext;
    public String agentId;
    public BackendlessUser agentUser;
    /**
     * View holder class
     */
    public class MyViewHolder extends RecyclerView.ViewHolder {
        
        public TextView name;
        public TextView email;
        public TextView phone;
        public CustomFontButton emailBTN;
        public CustomFontButton phoneBTN;
        public CustomFontButton deleteUserBTN;
        public View rootView;
        public Boolean isWeCanDelete = false;
        
        public FrameLayout rootLayout;
        
        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.tv_name_pop);
            email = (TextView) view.findViewById(R.id.tv_email_pop);
            phone = (TextView) view.findViewById(R.id.tv_phone_pop);
            
            emailBTN = (CustomFontButton) view.findViewById(R.id.bt_call_pop);
            phoneBTN = (CustomFontButton) view.findViewById(R.id.bt_sms_pop);
            deleteUserBTN = (CustomFontButton) view.findViewById(R.id.bt_decline_pop);
            
            rootLayout = (FrameLayout) view.findViewById(R.id.root_pop);
            rootView = view;
        }
    }
    
    /**
     * End of ViewHolder Class
     */
    
    public AdapterForAgentCard(List<BackendlessUser> response, Context context, BackendlessUser user) {
        this.mResponse = response;
        this.mContext = context;
        this.agentUser = user;
        this.agentId = agentUser.getUserId();
    }
    
    
    @Override
    public int getItemCount() {
        return mResponse.size();
    }
    
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext)//parent.getContext())
                .inflate(R.layout.card_for_agent, parent, false);
        
        return new MyViewHolder(v);
    }
    
    Boolean isWeCanDelete;
    ProgressDialog progressDialog;
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final BackendlessUser backendlessUser = mResponse.get(position);
        isWeCanDelete = holder.isWeCanDelete;
        holder.name.setText(String.valueOf(backendlessUser.getProperty("name")));
        holder.email.setText("Email: " + backendlessUser.getEmail());
        holder.phone.setText("Phone: " + String.valueOf(backendlessUser.getProperty("phone")));
        
        //setUpOnClick
        holder.emailBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToEmail(backendlessUser);
            }
        });
        holder.phoneBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToPhoneCall(backendlessUser, v);
            }
        });
        holder.deleteUserBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isWeCanDelete) {
                    progressDialog = new ProgressDialog(v.getContext());
                    progressDialog.setCancelable(true);
                    progressDialog.setMessage("Deleting in process..");
                    progressDialog.show();
                    
                    getPendingInDatabase(backendlessUser, mResponse, holder.getAdapterPosition());
                    isWeCanDelete = true;
                } else Toast.makeText(v.getContext(), "Wait", Toast.LENGTH_SHORT).show();
            }
        });
    }
    
    private void goToEmail(BackendlessUser backendlessUser) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{backendlessUser.getEmail()});
        i.putExtra(Intent.EXTRA_SUBJECT, "");
        i.putExtra(Intent.EXTRA_TEXT, "");
        try {
            mContext.startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(mContext, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }
    
    private void goToPhoneCall(BackendlessUser backendlessUser, View v) {
        if (String.valueOf(backendlessUser.getProperty("phone")) != null) {
            String uri = "tel:" + String.valueOf(backendlessUser.getProperty("phone"));
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse(uri));
            mContext.startActivity(intent);
        } else {
            Toast.makeText(v.getContext(), "Can't call.", Toast.LENGTH_SHORT).show();
        }
    }
    
    private void getPendingInDatabase(final BackendlessUser backendlessUser, final List<BackendlessUser> mResponse, final int position) {
        /*Backendless.UserService.findById(agentId, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser currentUser) {
                HashMap<String, Object> parentObject = new HashMap<String, Object>();
                parentObject.put("objectId", currentUser.getObjectId());
                
                HashMap<String, Object> childObject = new HashMap<String, Object>();
                childObject.put("objectId", backendlessUser.getObjectId());
                
                ArrayList<Map> children = new ArrayList<Map>();
                children.add(childObject);
                
                deleteRelationFromDatabaseAndNotifiView(currentUser, children, mResponse, position);
            }
            
            @Override
            public void handleFault(BackendlessFault fault) {
                
            }
        });*/

        final AsyncCallback<BackendlessCollection<Map>> callback = new AsyncCallback<BackendlessCollection<Map>>() {
            @Override
            public void handleResponse(BackendlessCollection<Map> users) {
                Log.e("PPPTotal obj - ", "" + users.getTotalObjects());


                Map<String, Object> map = users.getData().get(0);
                map.put("status", "contacted");
                final AsyncCallback<Map> callback = new AsyncCallback<Map>() {
                    @Override
                    public void handleResponse(Map maps) {
                        Log.e("Total Obj an err - ", "" + maps.toString());
                        if(progressDialog != null && progressDialog.isShowing())
                            progressDialog.cancel();
                        mResponse.remove(position);
                        notifyItemRemoved(position);

                    }

                    @Override
                    public void handleFault(BackendlessFault backendlessFault) {
                        Toast.makeText(mContext, "Connect error " + backendlessFault.getMessage(), Toast.LENGTH_LONG).show();

                    }
                };
                Backendless.Data.of("Inbox").save(map, callback);


//                loadRelationsQueryBuilder.prepareNextPage();
            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {
                Toast.makeText(mContext, "Contract opt error " + backendlessFault.getMessage(), Toast.LENGTH_LONG).show();
            }
        };

        BackendlessDataQuery dataQuery = new BackendlessDataQuery();
        StringBuilder whereClause = new StringBuilder();
        whereClause.append("status = 'pending'");
        whereClause.append(" and ");
        whereClause.append("sender.objectId = '" + backendlessUser.getUserId() + "'");
        whereClause.append(" and ");
        whereClause.append("receiver.objectId = '" + agentId + "'");
        dataQuery.setWhereClause(whereClause.toString());
        Backendless.Data.of("Inbox").find(dataQuery, callback);

    }
    
    private void deleteRelationFromDatabaseAndNotifiView(BackendlessUser currentUser, ArrayList<Map> children, final List<BackendlessUser> mResponse, final int position) {
        /*
        Backendless.Data.of(BackendlessUser.class).deleteRelation(currentUser, "pending", children, new AsyncCallback<Integer>() {
            @Override
            public void handleResponse(Integer response) {
                mResponse.remove(position);
                notifyDataSetChanged();
                progressDialog.dismiss();
            }
            
            @Override
            public void handleFault(BackendlessFault fault) {
                
            }
        });*/
    }
    
}

