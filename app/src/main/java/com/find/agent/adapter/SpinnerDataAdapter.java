package com.find.agent.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.find.agent.R;


/**
 * Created by iBuildX on 12/8/2016.
 */

public class SpinnerDataAdapter extends ArrayAdapter {

    private Context context;
    private String[] dataArray;

    public SpinnerDataAdapter(Context context, int textViewResourceId, String[] _dataArray) {
        super(context, textViewResourceId, _dataArray);
        this.context = context;
        this.dataArray = _dataArray;
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        // Inflating the layout for the custom Spinner
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_spinner_layout, parent, false);

        // Declaring and Typecasting the textview in the inflated layout
        TextView tvLanguage = (TextView) layout
                .findViewById(R.id.tvLanguage);

        // Setting the text using the array
        tvLanguage.setText(dataArray[position]);

        // Setting the color of the text
        tvLanguage.setTextColor(Color.WHITE);

        return layout;
    }

    // It gets a View that displays in the drop down popup the data at the specified position
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // It gets a View that displays the data at the specified position
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
}


