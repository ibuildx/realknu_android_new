package com.find.agent.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.find.agent.R;
import com.find.agent.fonts.CustomFontTextView;
import com.find.agent.model.CityModel;

import java.util.ArrayList;
import java.util.Locale;

public class CustomizedSpinnerAdapter extends ArrayAdapter<CityModel>

{
    Activity context;
    ArrayList<CityModel> objects;
    private ArrayList<CityModel> arraylist;
    public CustomizedSpinnerAdapter(Activity context, int textViewResourceId,
                                    ArrayList<CityModel> objects) {
        super(context, textViewResourceId, objects);

        this.context = context;
        this.objects = objects;
        arraylist = new ArrayList<>();
        arraylist.addAll(objects);
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {

        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View row = inflater.inflate(R.layout.row_spinner, parent, false);
        CustomFontTextView label = (CustomFontTextView) row.findViewById(R.id.txt_title);
        label.setText(objects.get(position).getName());


        return row;
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        //return super.getView(position, convertView, parent);

        LayoutInflater inflater = context.getLayoutInflater();
        View row = inflater.inflate(R.layout.dropdown_item, parent, false);
        CustomFontTextView label = (CustomFontTextView) row.findViewById(R.id.txt_title);
        label.setText(objects.get(position).getName());


        return row;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        objects.clear();
        if (charText.length() == 0) {
            objects.addAll(arraylist);
        }
        else
        {
            for (CityModel wp : arraylist)
            {
                if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText))
                {
                    objects.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

}
