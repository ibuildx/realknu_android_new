package com.find.agent.dialog;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.find.agent.R;
import com.find.agent.fonts.CustomFontButton;
import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.WRITE_CONTACTS;
import static com.find.agent.activity.AgentQuestionsActivity.COMPANY_NAME;
import static com.find.agent.activity.AgentQuestionsActivity.LANGUAGE;
import static com.find.agent.activity.AgentQuestionsActivity.LICENSE_NUMBER;
import static com.find.agent.activity.AgentQuestionsActivity.SPECIALITY;
import static com.find.agent.activity.ChooseUserActivity.CUSTOMER_PREFERENCES;
import static com.find.agent.activity.CustomerMapsActivity.CUSTOMER_PREFERENCES_FACEBOOK;
import static com.find.agent.activity.CustomerMapsActivity.EMAIL;
import static com.find.agent.activity.CustomerMapsActivity.NameCustomer;
import static com.find.agent.activity.CustomerMapsActivity.ObjectId;
import static com.find.agent.activity.CustomerMapsActivity.PHONE;
import static com.find.agent.activity.LoginActivity.FACEBOOK_LOGIN_AND_REGISTRATION;
import static com.find.agent.service.AddAgentToMap.AGENT;

;


/**
 * Created by diha-o@github.com on 5/31/2016.
 */
public class MarkerDialogPopUpActivity extends Activity {

    Button setCategoryButton;
    ImageButton closeCatggegoryDialog;

    TextView agentType;
    TextView nameTextView;
    TextView emailTextView;
    TextView phoneTextView;
    TextView licenseTextView;
    TextView companyTextView;
    TextView languageTextView;
    TextView specialityTextView;
    DilatingDotsProgressBar progressBarAcceptBTN, progressBarDeclineBTN;


    CustomFontButton callButton;
    CustomFontButton smsButton, whatsappButton;
    CustomFontButton acceptButton;
    CustomFontButton declineButton;

    private String name;
    private String email;
    private String phone;
    private String companyName;
    private String licenseNumber;
    private String language;
    private String speciality;

    private String objectIdAgent;
    private String objectIdCustomer;

    Boolean isPending;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_select_city_state);

        initView();

        getExtrasAndSetUpTextView();
    }

    private void initView() {
        SharedPreferences sharedPreferencesCustomer;
        if (!FACEBOOK_LOGIN_AND_REGISTRATION)
            sharedPreferencesCustomer = getSharedPreferences(CUSTOMER_PREFERENCES, Context.MODE_PRIVATE);
        else
            sharedPreferencesCustomer = getSharedPreferences(CUSTOMER_PREFERENCES_FACEBOOK, Context.MODE_PRIVATE);
        objectIdCustomer = sharedPreferencesCustomer.getString(ObjectId, "null");

        progressBarAcceptBTN = (DilatingDotsProgressBar) findViewById(R.id.progressBarAcceptBTN);
        progressBarDeclineBTN = (DilatingDotsProgressBar) findViewById(R.id.progressBarDeclineBTN);
        showAcceptButtonProgress();

        agentType = (TextView) findViewById(R.id.tv_agent_type);
        nameTextView = (TextView) findViewById(R.id.tv_name_pop);
        emailTextView = (TextView) findViewById(R.id.tv_email_pop);
        phoneTextView = (TextView) findViewById(R.id.tv_phone_pop);
        licenseTextView = (TextView) findViewById(R.id.tv_license_pop);
        companyTextView = (TextView) findViewById(R.id.tv_company_pop);
        languageTextView = (TextView) findViewById(R.id.tv_language_pop);
        specialityTextView = (TextView) findViewById(R.id.tv_speciality_pop);

        whatsappButton = (CustomFontButton) findViewById(R.id.bt_whatsapp_pop);
        smsButton = (CustomFontButton) findViewById(R.id.bt_sms_pop);
        callButton = (CustomFontButton) findViewById(R.id.bt_call_pop);
        acceptButton = (CustomFontButton) findViewById(R.id.bt_accept_pop);
        declineButton = (CustomFontButton) findViewById(R.id.bt_decline_pop);

        setCategoryButton = (Button) findViewById(R.id.dialog_set_tour_language_button);
        closeCatggegoryDialog = (ImageButton) findViewById(R.id.dialog_close_image_button);

        whatsappButton.setOnClickListener(callOnClick);
        smsButton.setOnClickListener(onClickSms);
        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + phone));
                    if (ActivityCompat.checkSelfPermission(MarkerDialogPopUpActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(callIntent);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        acceptButton.setOnClickListener(acceptOnClick);
        declineButton.setOnClickListener(declineOnClick);
        closeCatggegoryDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MarkerDialogPopUpActivity.this.finish();
                MarkerDialogPopUpActivity.this.overridePendingTransition(0, 0);
            }
        });
    }

    private void getExtrasAndSetUpTextView() {
        name = getIntent().getExtras().getString(NameCustomer);
        email = getIntent().getExtras().getString(EMAIL);
        phone = getIntent().getExtras().getString(PHONE);
        companyName = getIntent().getExtras().getString(COMPANY_NAME);
        licenseNumber = getIntent().getExtras().getString(LICENSE_NUMBER);
        language = getIntent().getExtras().getString(LANGUAGE);
        speciality = getIntent().getExtras().getString(SPECIALITY);
        objectIdAgent = getIntent().getExtras().getString(AGENT);

        //agentType.setText(getIntent().getExtras().getString("agentType"));
        nameTextView.setText(name);
        emailTextView.setText("Email: " + email);
        phoneTextView.setText("Phone: " + phone);
        licenseTextView.setText("License No: " + licenseNumber);
        companyTextView.setText("Company: " + companyName);
        languageTextView.setText("Language:"+'\n'+  language);
//        specialityTextView.setText("Speciality: " + speciality);
        specialityTextView.setText("Speciality"+'\n'+getIntent().getExtras().getString("agentType"));

        loadRelationWhenViewOnCreate();
    }

    public void showAcceptButtonProgress() {
        progressBarAcceptBTN.setVisibility(View.VISIBLE);
        progressBarAcceptBTN.showNow();
    }

    public void hideAcceptButtonProgress() {
        progressBarAcceptBTN.setVisibility(View.INVISIBLE);
        progressBarAcceptBTN.hideNow();
    }

    public void showDeclineButtonProgress() {
        progressBarDeclineBTN.setVisibility(View.VISIBLE);
        progressBarDeclineBTN.showNow();
    }

    public void hideDeclineButtonProgress() {
        progressBarDeclineBTN.setVisibility(View.INVISIBLE);
        progressBarDeclineBTN.hideNow();
    }

    private void loadRelationWhenViewOnCreate() {

        final AsyncCallback<BackendlessCollection<Map>> callback = new AsyncCallback<BackendlessCollection<Map>>() {
            @Override
            public void handleResponse(BackendlessCollection<Map> users) {
                Log.e("PPPTotal obj - ", "" + users.getTotalObjects());

                hideAcceptButtonProgress();

                if (users.getData().size() > 0) {
                    acceptButton.setText("Pending");
                    declineButton.setClickable(true);
                    isPending = true;
                } else {
                    acceptButton.setText("Accept");
                    declineButton.setClickable(false);
                    isPending = false;
                }

//                loadRelationsQueryBuilder.prepareNextPage();
            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {
                Toast.makeText(MarkerDialogPopUpActivity.this, "Accept error " + backendlessFault.getMessage(), Toast.LENGTH_LONG).show();
            }
        };

        BackendlessDataQuery dataQuery = new BackendlessDataQuery();
        StringBuilder whereClause = new StringBuilder();
        whereClause.append("status = 'pending'");
        whereClause.append(" and ");
        whereClause.append("sender.objectId = '" + objectIdCustomer + "'");
        whereClause.append(" and ");
        whereClause.append("receiver.objectId = '" + objectIdAgent + "'");
        dataQuery.setWhereClause(whereClause.toString());
        Backendless.Data.of("Inbox").find(dataQuery, callback);
    }

    private void addOrDeleteRelationChecker(final Boolean addAndDeleteRelation) {
        Backendless.UserService.findById(objectIdAgent, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser retrivedAgent) {
                HashMap<String, Object> parentObject = new HashMap<String, Object>();
                parentObject.put("objectId", retrivedAgent.getObjectId());

                HashMap<String, Object> childObject = new HashMap<String, Object>();
                childObject.put("objectId", objectIdCustomer);

                ArrayList<Map> children = new ArrayList<Map>();
                children.add(childObject);

                if (addAndDeleteRelation) {
                    showAcceptButtonProgress();
                    addRelarion(retrivedAgent, children);
                } else {
                    showDeclineButtonProgress();
                    deleteRelation();
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Toast.makeText(MarkerDialogPopUpActivity.this, "Decline error " + fault.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void addRelarion(final BackendlessUser retrivedAgent, final ArrayList<Map> children) {

        BackendlessDataQuery currentUserQuery = new BackendlessDataQuery();
        currentUserQuery.setWhereClause("objectId = '" + children.get(0).get("objectId").toString() + "'");

        final AsyncCallback<BackendlessCollection<BackendlessUser>> callbackUser = new AsyncCallback<BackendlessCollection<BackendlessUser>>() {
            @Override
            public void handleResponse(BackendlessCollection<BackendlessUser> user) {
                Log.e("Total Obj an err - ", "" + user);

                Map<String, Object> map = new HashMap<>();
                map.put("status", "pending");
                map.put("receiver", retrivedAgent); // Agent
                map.put("sender", user.getData().get(0)); // Customer
                final AsyncCallback<Map> callback = new AsyncCallback<Map>() {
                    @Override
                    public void handleResponse(Map maps) {
                        Log.e("Total Obj an err - ", "" + maps.toString());

                        acceptButton.setText("Pending");
                        declineButton.setClickable(true);

                        AlertDialog.Builder builder = new AlertDialog.Builder(MarkerDialogPopUpActivity.this);
                        builder.setMessage("Connect Request Send!");
                        builder.setNegativeButton("", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();

                        isPending = true;
                        hideAcceptButtonProgress();
                        acceptButton.setClickable(true);
                    }

                    @Override
                    public void handleFault(BackendlessFault backendlessFault) {
                        Toast.makeText(MarkerDialogPopUpActivity.this, "Connect error " + backendlessFault.getMessage(), Toast.LENGTH_LONG).show();
                        hideAcceptButtonProgress();
                        acceptButton.setClickable(true);
                    }
                };
                Backendless.Data.of("Inbox").save(map, callback);
            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {
                Toast.makeText(MarkerDialogPopUpActivity.this, "Connect error " + backendlessFault.getMessage(), Toast.LENGTH_LONG).show();
                hideAcceptButtonProgress();
                acceptButton.setClickable(true);
            }
        };
        Backendless.Data.of(BackendlessUser.class).find(currentUserQuery, callbackUser);
    }

    private void deleteRelation() {

        final AsyncCallback<BackendlessCollection<Map>> callback = new AsyncCallback<BackendlessCollection<Map>>() {
            @Override
            public void handleResponse(BackendlessCollection<Map> user) {
                Log.e("PPPTotal obj - ", "" + user.getTotalObjects());

                if (user.getData().size() > 0) {
                    acceptButton.setText("Accept");
                    declineButton.setClickable(false);
                    // New contact object has been saved, now it can be deleted
                    Backendless.Data.of("Inbox").remove(user.getData().get(0), new AsyncCallback<Long>() {
                        @Override
                        public void handleResponse(Long response) {
                            acceptButton.setText("Accept");
                            declineButton.setClickable(false);

                            AlertDialog.Builder builder = new AlertDialog.Builder(MarkerDialogPopUpActivity.this);
                            builder.setMessage("Decline Success!");
                            builder.setNegativeButton("", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();

                            isPending = false;
                            hideDeclineButtonProgress();
                            declineButton.setClickable(true);
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                            // an error has occurred, the error code can be retrieved with fault.getCode()
                            hideDeclineButtonProgress();
                            declineButton.setClickable(true);
                        }
                    });
                }
            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {
                Log.e("Server report an err - ", "" + backendlessFault.getMessage());
                hideDeclineButtonProgress();
                declineButton.setClickable(true);
            }
        };

        BackendlessDataQuery dataQuery = new BackendlessDataQuery();
        StringBuilder whereClause = new StringBuilder();
        whereClause.append("status = 'pending'");
        whereClause.append(" and ");
        whereClause.append("sender.objectId = '" + objectIdCustomer + "'");
        whereClause.append(" and ");
        whereClause.append("receiver.objectId = '" + objectIdAgent + "'");
        dataQuery.setWhereClause(whereClause.toString());
        Backendless.Data.of("Inbox").find(dataQuery, callback);
    }

    View.OnClickListener onClickSms = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String number = phone;  // The number on which you want to send SMS
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));


        }
    };

    View.OnClickListener callOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (checkPermission()) {

                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", phone);
                clipboard.setPrimaryClip(clip);

                if (contactExists(MarkerDialogPopUpActivity.this, phone)) {

                    String id = getContactId();
                    if (id == null)
                        Toast.makeText(MarkerDialogPopUpActivity.this, "Phone number copy to clipboard", Toast.LENGTH_LONG).show();
                    else
                        callOnWhatsapp(id);

                } else {
                    AlertDialog.Builder phoneDialog = new AlertDialog.Builder(MarkerDialogPopUpActivity.this);
                    phoneDialog.setTitle("Add contact?");
                    phoneDialog.setMessage("Do you want to add this agent to your contact list?");
                    phoneDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

                            int rawContactInsertIndex = ops.size();
                            ops.add(ContentProviderOperation.newInsert(
                                    ContactsContract.RawContacts.CONTENT_URI)
                                    .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                                    .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                                    .build());
                            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex)
                                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                                    .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, name)
                                    .build());
                            ops.add(ContentProviderOperation.
                                    newInsert(ContactsContract.Data.CONTENT_URI)
                                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex)
                                    .withValue(ContactsContract.Data.MIMETYPE,
                                            ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, phone)
                                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                                            ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                                    .build());
                            try {
                                getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
                                if (!openApp(MarkerDialogPopUpActivity.this, "com.whatsapp")) {
                                    Toast.makeText(MarkerDialogPopUpActivity.this, "Probably you don't install watsapp", Toast.LENGTH_LONG).show();

                                } else {
                                    String id = getContactId();
                                    if (id == null)
                                        Toast.makeText(MarkerDialogPopUpActivity.this, "Phone number copy to clipboard", Toast.LENGTH_LONG).show();
                                    else
                                        callOnWhatsapp(id);
                                }

                            } catch (RemoteException e) {
                                e.printStackTrace();
                            } catch (OperationApplicationException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    phoneDialog.setNegativeButton("NO", null);
                    phoneDialog.show();
                }
            } else
                requestPermission();
        }
    };

    Cursor cursor;

    public void callOnWhatsapp(String id) {


        if (!openApp(MarkerDialogPopUpActivity.this, "com.whatsapp")) {
            Toast.makeText(MarkerDialogPopUpActivity.this, "Probably you don't install watsapp", Toast.LENGTH_LONG).show();

        } else {
            String whatsappcall = "https://api.whatsapp.com/send?phone=" + (phone.startsWith("+1") ? phone : "1" + phone) + "&text=" + name;
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(whatsappcall));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }






    public String getContactId() {
        String contactid26 = null;

        ContentResolver contentResolver = getContentResolver();

        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phone));

        Cursor cursor =
                contentResolver.query(
                        uri,
                        new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID},
                        null,
                        null,
                        null);

        if (cursor != null) {
            while (cursor.moveToNext()) {
                String contactName = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup.DISPLAY_NAME));
                contactid26 = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));

            }
            cursor.close();
        }
        return contactid26;
    }

    public static boolean openApp(Context context, String packageName) {
        PackageManager manager = context.getPackageManager();
        try {
            Intent i = manager.getLaunchIntentForPackage(packageName);
            if (i == null) {
                return false;
                //throw new PackageManager.NameNotFoundException();
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(MarkerDialogPopUpActivity.this, new String[]{WRITE_CONTACTS, CALL_PHONE}, PERMISSION_CODE_CALL);
    }

    public boolean checkPermission() {
        int contactRead = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_CONTACTS);
        int callPhone = ContextCompat.checkSelfPermission(getApplicationContext(), CALL_PHONE);
        return contactRead == PackageManager.PERMISSION_GRANTED && callPhone == PackageManager.PERMISSION_GRANTED;
    }

    public final static int PERMISSION_CODE_CALL = 1000;

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case PERMISSION_CODE_CALL:
                if (grantResults.length > 0) {
                    boolean NetworkStatePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (NetworkStatePermission) {
                        if (checkPermission())
                            callButton.performClick();
                    } else {
                        Toast.makeText(MarkerDialogPopUpActivity.this, "Permission Denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }


    public boolean contactExists(Context context, String number) {
/// number is the phone number
        Uri lookupUri = Uri.withAppendedPath(
                ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(number));
        String[] mPhoneNumberProjection = {ContactsContract.PhoneLookup._ID, ContactsContract.PhoneLookup.NUMBER, ContactsContract.PhoneLookup.DISPLAY_NAME};
        Cursor cur = context.getContentResolver().query(lookupUri, mPhoneNumberProjection, null, null, null);
        try {
            if (cur.moveToFirst()) {
                return true;
            }
        } finally {
            if (cur != null)
                cur.close();
        }
        return false;
    }

    View.OnClickListener acceptOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!isPending) {
                acceptButton.setClickable(false);
                addOrDeleteRelationChecker(true);
            }


        }
    };
    View.OnClickListener declineOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isPending) {
                declineButton.setClickable(false);
                addOrDeleteRelationChecker(false);
            }

        }
    };
}
