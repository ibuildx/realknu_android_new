package com.find.agent.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;

import com.find.agent.R;
import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;


/**
 * Created by Nabeel Hafeez on 10/31/2016.
 */
public class ProgressLoadingLocationDialog extends Dialog {

    Context activity;

    public ProgressLoadingLocationDialog(Context _activity) {
        super(_activity);
        this.activity = _activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.progress_loading_location_dialog);

        DilatingDotsProgressBar mDilatingDotsProgressBar = (DilatingDotsProgressBar) findViewById(R.id.progress_dots);

        // show progress bar and start animating
        mDilatingDotsProgressBar.showNow();


    }

}
