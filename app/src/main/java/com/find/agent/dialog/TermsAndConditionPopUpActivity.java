package com.find.agent.dialog;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

import com.find.agent.R;
import com.find.agent.fonts.CustomFontButton;

/**
 * Created by leaditteam on 01.06.17.
 */

public class TermsAndConditionPopUpActivity extends Activity {
    ImageButton closeCategoryDialog;
    CustomFontButton acceptBtn;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.term_and_condition_pop_up);
    
        closeCategoryDialog = (ImageButton) findViewById(R.id.dialog_close_image_button);
        closeCategoryDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED);
                TermsAndConditionPopUpActivity.this.finish();
                TermsAndConditionPopUpActivity.this.overridePendingTransition(0, 0);
            }
        });
        acceptBtn = (CustomFontButton) findViewById(R.id.bt_decline_pop);
        acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_OK);
                TermsAndConditionPopUpActivity.this.finish();
                TermsAndConditionPopUpActivity.this.overridePendingTransition(0, 0);
            }
        });
    }
}
