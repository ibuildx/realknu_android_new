package com.find.agent.utility;

import android.app.Activity;
import android.net.ConnectivityManager;
import android.widget.Toast;

import com.find.agent.R;

/**
 * Provides values used to communicate with backend.
 */
public class BackendSettings {
    public static final String VERSION_NAME_BACKENDLESS = "v1";
    public static final String APPLICATION_ID = "306134D4-1ACC-0A6C-FF03-18DFC2BB9900";
    public static final String ANDROID_SECRET_KEY = "A984CF04-21B1-F496-FF78-68270A3BD800";
    public static final String VERSION = "v1";
    public static final String SERVER_URL = "https://api.backendless.com";
    public static final String API_TO_BACKENDLESS_IMAGE_STRING = "/files/usersPhoto/";

    //SET YOUR GOOGLE PROJECT ID BEFORE LAUNCH
    //ALSO YOU SHOULD SET GOOGLE API KEY IN BACKENDLESS CONSOLE
    public static final String GOOGLE_PROJECT_ID = "";
    public static final String DEFAULT_CHANNEL = "default";

    public static boolean isNetworkConnected(Activity activity) {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(android.content.Context.CONNECTIVITY_SERVICE);

        if (cm.getActiveNetworkInfo() == null) {
            Toast.makeText(activity, activity.getString(R.string.no_connection), Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

}
