package com.find.agent.app;

import android.app.Application;
import android.os.SystemClock;

import com.backendless.Backendless;
import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.find.agent.utility.BackendSettings;

import io.fabric.sdk.android.Fabric;

/**
 * Created by iBuildX on 1/11/2017.
 */

public class SplashApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
        SystemClock.sleep(3000);
    }
}
