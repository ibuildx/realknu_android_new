package com.find.agent.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.util.Log;

import com.backendless.BackendlessUser;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.find.agent.activity.AgentQuestionsActivity.COMPANY_NAME;
import static com.find.agent.activity.AgentQuestionsActivity.FIRST_NAME;
import static com.find.agent.activity.AgentQuestionsActivity.LANGUAGE;
import static com.find.agent.activity.AgentQuestionsActivity.LAST_NAME;
import static com.find.agent.activity.AgentQuestionsActivity.LICENSE_NUMBER;
import static com.find.agent.activity.AgentQuestionsActivity.SPECIALITY;
import static com.find.agent.activity.CustomerMapsActivity.EMAIL;
import static com.find.agent.activity.CustomerMapsActivity.NameCustomer;
import static com.find.agent.activity.CustomerMapsActivity.ObjectId;
import static com.find.agent.activity.CustomerMapsActivity.PHONE;
import static com.find.agent.service.AddAgentToMap.AGENT;

/**
 * Created by leaditteam on 07.06.17.
 */

public class AppHepler {
    
    /**
     *Backendless user helper
     */
    public BackendlessUser parseMapToBackendlessUser(List<Map> map, int i) {
        BackendlessUser backendlessUser = new BackendlessUser();
        BackendlessUser sender = (BackendlessUser) map.get(i).get("sender");
        backendlessUser.setProperty("firstName", sender.getProperty("firstName"));
        backendlessUser.setProperty("LastName", sender.getProperty("LastName"));
        backendlessUser.setProperty("name", sender.getProperty("name"));
        backendlessUser.setProperty("phone", sender.getProperty("phone"));
        backendlessUser.setProperty("latitude", sender.getProperty("latitude"));
        backendlessUser.setProperty("longitude", sender.getProperty("longitude"));
        backendlessUser.setEmail(String.valueOf(sender.getProperty("email")));
        backendlessUser.setProperty("objectId", sender.getProperty("objectId"));
        
        return backendlessUser;
    }

    /*
     public static final String FIRST_AND_LASTNAME = "QfistAndLastName";
    public static final String COMPANY_NAME = "companyName";
    public static final String LICENSE_NUMBER = "licenceNumber";
    public static final String PHONE_NUMBER = "bestPhoneNo";
    public static final String LANGUAGE = "Lenguage";
    public static final String SPECIALITY = "speciality";
     */

    public HashMap getDataAndSetToTempMap(BackendlessUser userData) {
        HashMap tempHashMap = new HashMap();
        try {
            String name = (userData.getProperty(FIRST_NAME) != null ? userData.getProperty(FIRST_NAME).toString() + " " : "") + (userData.getProperty(LAST_NAME) != null ? userData.getProperty(LAST_NAME).toString() : "");
//            String name         = userData.getProperty("name") != null ? userData.getProperty("name").toString() : "";
            double latitude     = userData.getProperty("latitude") != null ? Double.parseDouble(userData.getProperty("latitude").toString()) : 0;
            double longitude    = userData.getProperty("longitude") != null ? Double.parseDouble(userData.getProperty("longitude").toString()) : 0;
            String email        = userData.getProperty("email") != null ? userData.getProperty("email").toString() : "";
            String phone        = userData.getProperty("bestPhoneNo") != null ? userData.getProperty("bestPhoneNo").toString() : "";
            String companyName  = userData.getProperty("companyName") != null ? userData.getProperty("companyName").toString() : "";
            String licenseNumber = userData.getProperty("licenceNumber") != null ? userData.getProperty("licenceNumber").toString() : "";
            String language     = userData.getProperty("Lenguage") != null ? userData.getProperty("Lenguage").toString() : "";
            String speciality   = userData.getProperty("speciality") != null ? userData.getProperty("speciality").toString() : "";
            Boolean isOnline    = userData.getProperty("agentIsOnline") != null ? Boolean.parseBoolean(userData.getProperty("agentIsOnline").toString()) : false;
            String agentType       = userData.getProperty("agentType") != null ? userData.getProperty("agentType").toString() : "";

            tempHashMap.put(NameCustomer, name);
            tempHashMap.put(EMAIL, email);
            tempHashMap.put(PHONE, phone);
            tempHashMap.put(COMPANY_NAME, companyName);
            tempHashMap.put(LICENSE_NUMBER, licenseNumber);
            tempHashMap.put(LANGUAGE, language);
            tempHashMap.put(SPECIALITY, speciality);
            tempHashMap.put("lat", latitude);
            tempHashMap.put("lot", longitude);
            tempHashMap.put(AGENT, userData.getProperty(ObjectId).toString());
            tempHashMap.put("agentIsOnline", isOnline);
            tempHashMap.put("agentType", agentType);
        } catch (Exception ex) {
            Log.e("TAG", ex.toString());
        }
        return tempHashMap;
    }
    
    /**
     * Location Helper
     */
    public MarkerOptions getMarker(double lat, double lot, int drawable) {
        LatLng latLng = new LatLng(lat, lot);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.icon(BitmapDescriptorFactory.fromResource(drawable));
        return markerOptions;
    }
    
    public Boolean checkIfItsInOurRadius(double latAgent, double lotAgent, double latMe, double lotMe) {
        Location agent = new Location("agentLocation");
        agent.setLatitude(latAgent);
        agent.setLongitude(lotAgent);
        
        Location me = new Location("meLocation");
        me.setLongitude(lotMe);
        me.setLatitude(latMe);
        
        double distance = me.distanceTo(agent);
        if (distance <= 32187)
            return true; //20 mil = 32187 meters
        else
            return false;
    }
    
    /**
     *Get image url helper
     */
    public Bitmap getThumbnail(Uri uri, Context context) throws FileNotFoundException, IOException {
        InputStream input = context.getContentResolver().openInputStream(uri);
        
        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither = true;//optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();
        
        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1)) {
            return null;
        }
        
        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;
        
        double ratio = (originalSize > 300) ? (originalSize / 300) : 1.0;
        
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither = true; //optional
        bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//
        input = context.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }
    
    private int getPowerOfTwoForSampleRatio(double ratio) {
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        if (k == 0) return 1;
        else return k;
    }
    
}
