package com.find.agent.model;

import com.backendless.Backendless;
//import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.geo.GeoPoint;
import com.backendless.persistence.BackendlessDataQuery;


import java.util.List;

public class Customer
{
  private String currentlong;
  private String ownerId;
  private String objectId;
  private java.util.Date updated;
  private String currentlat;
  private String firstname;
  private String phone;
  private String email;
  private java.util.Date created;
  private String lastname;
  public String getCurrentlong()
  {
    return currentlong;
  }

  public void setCurrentlong( String currentlong )
  {
    this.currentlong = currentlong;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

  public String getObjectId()
  {
    return objectId;
  }

  public java.util.Date getUpdated()
  {
    return updated;
  }

  public String getCurrentlat()
  {
    return currentlat;
  }

  public void setCurrentlat( String currentlat )
  {
    this.currentlat = currentlat;
  }

  public String getFirstname()
  {
    return firstname;
  }

  public void setFirstname( String firstname )
  {
    this.firstname = firstname;
  }

  public String getPhone()
  {
    return phone;
  }

  public void setPhone( String phone )
  {
    this.phone = phone;
  }

  public String getEmail()
  {
    return email;
  }

  public void setEmail( String email )
  {
    this.email = email;
  }

  public java.util.Date getCreated()
  {
    return created;
  }

  public String getLastname()
  {
    return lastname;
  }

  public void setLastname( String lastname )
  {
    this.lastname = lastname;
  }

                                                    
  public Customer save()
  {
    return Backendless.Data.of( Customer.class ).save( this );
  }

  public Future<Customer> saveAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Customer> future = new Future<Customer>();
      Backendless.Data.of( Customer.class ).save( this, future );

      return future;
    }
  }

  public void saveAsync( AsyncCallback<Customer> callback )
  {
    Backendless.Data.of( Customer.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( Customer.class ).remove( this );
  }

  public Future<Long> removeAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Long> future = new Future<Long>();
      Backendless.Data.of( Customer.class ).remove( this, future );

      return future;
    }
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( Customer.class ).remove( this, callback );
  }

  public static Customer findById( String id )
  {
    return Backendless.Data.of( Customer.class ).findById( id );
  }

  public static Future<Customer> findByIdAsync( String id )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Customer> future = new Future<Customer>();
      Backendless.Data.of( Customer.class ).findById( id, future );

      return future;
    }
  }

  public static void findByIdAsync( String id, AsyncCallback<Customer> callback )
  {
    Backendless.Data.of( Customer.class ).findById( id, callback );
  }

  public static Customer findFirst()
  {
    return Backendless.Data.of( Customer.class ).findFirst();
  }

  public static Future<Customer> findFirstAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Customer> future = new Future<Customer>();
      Backendless.Data.of( Customer.class ).findFirst( future );

      return future;
    }
  }

  public static void findFirstAsync( AsyncCallback<Customer> callback )
  {
    Backendless.Data.of( Customer.class ).findFirst( callback );
  }

  public static Customer findLast()
  {
    return Backendless.Data.of( Customer.class ).findLast();
  }

  public static Future<Customer> findLastAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Customer> future = new Future<Customer>();
      Backendless.Data.of( Customer.class ).findLast( future );

      return future;
    }
  }

  public static void findLastAsync( AsyncCallback<Customer> callback )
  {
    Backendless.Data.of( Customer.class ).findLast( callback );
  }


  /*public static List<Customer> find(DataQueryBuilder query )
  {
    return Backendless.Data.of( Customer.class ).find( query );
  }

  public static Future<List<Customer>> findAsync( DataQueryBuilder query )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<List<Customer>> future = new Future<List<Customer>>();
      Backendless.Data.of( Customer.class ).find( query, future );

      return future;
    }
  }

  public static void findAsync( DataQueryBuilder query, AsyncCallback<List<Customer>> callback )
  {
    Backendless.Data.of( Customer.class ).find( query, callback );
  }*/
}