package com.find.agent;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.util.Log;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

public class MyService  extends Service {


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    String agentId = "";
    BackendlessUser backendlessUser;
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        agentId = intent.getStringExtra("agentId");
        Backendless.UserService.findById(agentId, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser response) {
                backendlessUser = response;
                backendlessUser.setProperty("agentIsOnline", false);
                backendlessUser.setProperty("loginStatus", false? "loggedIn" : "loggedOff");
            }
            @Override
            public void handleFault(BackendlessFault backendlessFault) {
                Log.d("error handleFault 57", "done");
            }
        });

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d("onTaskRemovedcalled", "called");

        super.onDestroy();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.d("onTaskRemovedcalled", "called");

        super.onTaskRemoved(rootIntent);

    }
}
