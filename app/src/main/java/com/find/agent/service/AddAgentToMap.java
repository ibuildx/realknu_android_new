package com.find.agent.service;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.find.agent.R;
import com.find.agent.activity.CustomerMapsActivity;
import com.find.agent.dialog.MarkerDialogPopUpActivity;
import com.find.agent.model.AppHepler;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static com.find.agent.activity.AgentQuestionsActivity.COMPANY_NAME;
import static com.find.agent.activity.AgentQuestionsActivity.LANGUAGE;
import static com.find.agent.activity.AgentQuestionsActivity.LICENSE_NUMBER;
import static com.find.agent.activity.AgentQuestionsActivity.SPECIALITY;
import static com.find.agent.activity.CustomerMapsActivity.AGENT_TYPE_ON_Ar;
import static com.find.agent.activity.CustomerMapsActivity.AGENT_TYPE_ON_RC;
import static com.find.agent.activity.CustomerMapsActivity.AGENT_TYPE_ON_RCR;
import static com.find.agent.activity.CustomerMapsActivity.AGENT_TYPE_ON_RR;
import static com.find.agent.activity.CustomerMapsActivity.EMAIL;
import static com.find.agent.activity.CustomerMapsActivity.NameCustomer;
import static com.find.agent.activity.CustomerMapsActivity.PHONE;

/**
 * Created by leaditteam on 23.05.17.
 */

public class AddAgentToMap extends Service {
    public static final String AGENT = "agent_id";

    IBinder LocalBinder = new LocalBinder();
    private ArrayList<BackendlessUser> userData = new ArrayList<>();
    private GoogleMap mMap;

    private HashMap<String, HashMap<String, String>> dataHashMap = new HashMap<>();

    private double myLat;
    private double myLot;

    private AppHepler appHepler = new AppHepler();

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return LocalBinder;
    }


    public void addMyLocationMarket() {
        MarkerOptions mp = new MarkerOptions();
        //mp.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        mp.position(new LatLng(myLat, myLot));
        mp.icon(BitmapDescriptorFactory
                .fromResource(R.drawable.ic_current_location));
        mp.title("Current Location");

        mMap.addMarker(mp);

    }


    class MyTask implements Runnable {

        @Override
        public void run() {
            try {
                getAgentList();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if ((mMap != null)) {
            //clearGoogleMap(mMap);
            //dataHashMap.clear();
            ScheduledThreadPoolExecutor exec = new ScheduledThreadPoolExecutor(1);
            long period = 30000; // the period between successive executions
            exec.scheduleAtFixedRate(new MyTask(), 0, period, TimeUnit.MILLISECONDS);

        }
        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * First of all, we need get the all list of all user in database
     */
    int containsSameObj = 0;
    boolean isFirstTime = true;
    int pageSize = 100, pageOffset = 0;

    private void getAgentList() throws InterruptedException {
        final AsyncCallback<BackendlessCollection<BackendlessUser>> callback = new AsyncCallback<BackendlessCollection<BackendlessUser>>() {
            @Override
            public void handleResponse(BackendlessCollection<BackendlessUser> users) {
                /*
                if (isFirstTime) {
                    Log.e("PPPTotal obj - ", "" + users.getTotalObjects());
                    isFirstTime = false;

                }

                if (userData.size() < users.getTotalObjects()) {

                    for (int p = 0; p < users.getData().size(); p++) {
                        if( contains(userData, users.getData().get(p).getProperty("objectId").toString()) )
                            containsSameObj++;
                        else {
                            userData.add(users.getData().get(p));
                        }
                    }

                    Log.e("PPPCurrent page obj - ", users.getCurrentPage().size() + "");
//                    userData.addAll(users.getData());
                    Log.e("PPPMain list size - ", userData.size() + " SameData : " + containsSameObj);
                    //pageOffset = pageOffset + pageSize;
//                    users.getPage(pageSize, pageOffset, this);//nextPage(this);
                }
                */

                Log.e("PPPCurrent page obj - ", users.getCurrentPage().size() + "");
//                    userData.addAll(users.getData());
                Log.e("PPPMain list size - ", userData.size() + " SameData : " + containsSameObj);
                userData = new ArrayList<>();
                userData.addAll(users.getData());
                addAgentToMap();
            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {
                Log.e("Server report an err - ", "" + backendlessFault.getMessage());
            }
        };
        BackendlessDataQuery dataQuery = new BackendlessDataQuery();
        dataQuery.setWhereClause("type = 'agent'");
        dataQuery.setWhereClause("loginStatus = 'loggedIn'");
        dataQuery.setPageSize(pageSize);
//        dataQuery.setOffset(pageOffset);
        Backendless.Data.of(BackendlessUser.class).find(dataQuery, callback);
    }

    boolean contains(ArrayList<BackendlessUser> list, String objId) {
        for (BackendlessUser item : list) {
            if (item.getObjectId().equals(objId)) {
                return true;
            }
        }
        return false;
    }

    /**
     * After we get list, need to check if current user is agent.
     */
    private void addAgentToMap() {
        dataHashMap.clear();
        clearGoogleMap(mMap);
        addMyLocationMarket();
        for (int i = 0; i < userData.size(); i++) {
            HashMap tempHashMap = appHepler.getDataAndSetToTempMap(userData.get(i));
            double agentLat = Double.parseDouble(tempHashMap.get("lat").toString());
            double agentLot = Double.parseDouble(tempHashMap.get("lot").toString());

            if (((myLot == 0) || (myLat == 0))) {//&&isOnline){
                cheAgentType(agentLat, agentLot, tempHashMap);
            }
            if ((appHepler.checkIfItsInOurRadius(agentLat, agentLot, myLat, myLot))) {//&& isOnline) {
                cheAgentType(agentLat, agentLot, tempHashMap);
            }
        }

    }

    int nil = 0;

    private void cheAgentType(double agentLat, double agentLot, HashMap tempHashMap) {
        Marker marker = null;
        nil++;
        Log.e("PPP", "AddMarker object : " + nil);

        if (tempHashMap.containsKey(LANGUAGE) && tempHashMap.get(LANGUAGE) != null && tempHashMap.get(LANGUAGE).toString().contains(CustomerMapsActivity.selectedLanguge)) {
            if (AGENT_TYPE_ON_RCR) {

               /* if (tempHashMap.get("agentType").toString().equalsIgnoreCase("RC"))
                    marker = mMap.addMarker(appHepler.getMarker(agentLat, agentLot, R.drawable.c_agent));
                else if (tempHashMap.get("agentType").toString().equalsIgnoreCase("RR"))
                    marker = mMap.addMarker(appHepler.getMarker(agentLat, agentLot, R.drawable.r_agent));
                else if (tempHashMap.get("agentType").toString().equalsIgnoreCase("Ar"))
                    marker = mMap.addMarker(appHepler.getMarker(agentLat, agentLot, R.drawable.ar_agent));
                else
                    marker = mMap.addMarker(appHepler.getMarker(agentLat, agentLot, R.drawable.rc_agent));

                addToMap(marker, tempHashMap);
            } else if (tempHashMap.get("agentType").toString().contains("RR") && AGENT_TYPE_ON_RR) {
                marker = mMap.addMarker(appHepler.getMarker(agentLat, agentLot, R.drawable.r_agent));
                addToMap(marker, tempHashMap);

            } else if (tempHashMap.get("agentType").toString().contains("Ar") && AGENT_TYPE_ON_Ar) {
                marker = mMap.addMarker(appHepler.getMarker(agentLat, agentLot, R.drawable.ar_agent));
                addToMap(marker, tempHashMap);

            } else if (tempHashMap.get("agentType").toString().contains("RC") && AGENT_TYPE_ON_RC) {
                marker = mMap.addMarker(appHepler.getMarker(agentLat, agentLot, R.drawable.c_agent));
                addToMap(marker, tempHashMap);

            } else {


            }*/

                if (tempHashMap.get("agentType").toString().equalsIgnoreCase("RC"))
                    marker = mMap.addMarker(appHepler.getMarker(agentLat, agentLot, R.drawable.ic_location));
                else if (tempHashMap.get("agentType").toString().equalsIgnoreCase("RR"))
                    marker = mMap.addMarker(appHepler.getMarker(agentLat, agentLot, R.drawable.ic_location));
                else if (tempHashMap.get("agentType").toString().equalsIgnoreCase("Ar"))
                    marker = mMap.addMarker(appHepler.getMarker(agentLat, agentLot, R.drawable.ic_location));
                else
                    marker = mMap.addMarker(appHepler.getMarker(agentLat, agentLot, R.drawable.ic_location));

                addToMap(marker, tempHashMap);
            } else if (tempHashMap.get("agentType").toString().contains("RR") && AGENT_TYPE_ON_RR) {
                marker = mMap.addMarker(appHepler.getMarker(agentLat, agentLot, R.drawable.ic_location));
                addToMap(marker, tempHashMap);

            } else if (tempHashMap.get("agentType").toString().contains("Ar") && AGENT_TYPE_ON_Ar) {
                marker = mMap.addMarker(appHepler.getMarker(agentLat, agentLot, R.drawable.ic_location));
                addToMap(marker, tempHashMap);

            } else if (tempHashMap.get("agentType").toString().contains("RC") && AGENT_TYPE_ON_RC) {
                marker = mMap.addMarker(appHepler.getMarker(agentLat, agentLot, R.drawable.ic_location));
                addToMap(marker, tempHashMap);

            } else {


            }

        }
    }

    private void addToMap(Marker marker, HashMap tempHashMap) {
        dataHashMap.put(marker.getId(), tempHashMap);
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (marker.getTitle() == null ) {
                    openDialogUser(dataHashMap.get(marker.getId()));
                    return false;
                } else
                    marker.showInfoWindow();

                return false;
            }
        });
    }


    public String getAgentTYpe(String typeString) {
        String agentType = "";
        String[] parts = typeString.split("\\s*,\\s*");
        for (int i = 0; i < parts.length; i++) {
            if (parts[i].equalsIgnoreCase("RR"))
                agentType = agentType + ",Residential";
            if (parts[i].equalsIgnoreCase("RC"))
                agentType = agentType + ",Commercial";
            if (parts[i].equalsIgnoreCase("Ar"))
                agentType = agentType + ",Rental";
        }
        if (agentType.startsWith(","))
            agentType = agentType.substring(1);

        return agentType;
    }

    private void openDialogUser(HashMap hashMap) {
        Intent intent = new Intent(this, MarkerDialogPopUpActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

        intent.putExtra("agentType", getAgentTYpe(hashMap.get("agentType").toString()));
        intent.putExtra(NameCustomer, hashMap.get(NameCustomer).toString());
        intent.putExtra(EMAIL, hashMap.get(EMAIL).toString());
        intent.putExtra(PHONE, hashMap.get(PHONE).toString());
        intent.putExtra(COMPANY_NAME, hashMap.get(COMPANY_NAME).toString());
        intent.putExtra(LICENSE_NUMBER, hashMap.get(LICENSE_NUMBER).toString());
        intent.putExtra(LANGUAGE, hashMap.get(LANGUAGE).toString());
        intent.putExtra(SPECIALITY, hashMap.get(SPECIALITY).toString());
        intent.putExtra(AGENT, hashMap.get(AGENT).toString());

        getApplication().startActivity(intent);
    }

    public void setGoogleMap(GoogleMap googleMap) {
        this.mMap = googleMap;
    }

    public void setMyLocation(Location myLocation) {
        if (myLocation != null) {
            myLat = myLocation.getLatitude();
            myLot = myLocation.getLongitude();
        } else {
            myLat = 0;
            myLot = 0;
        }
    }

    public void setMyLocation(LatLng myLocation) {
        if (myLocation != null) {
            myLat = myLocation.latitude;
            myLot = myLocation.longitude;
        } else {
            myLat = 0;
            myLot = 0;
        }
    }

    public void clearGoogleMap(GoogleMap googleMap) {
        if (googleMap != null)
            googleMap.clear();
    }

    /**
     * Local classes helper
     */
    public class LocalBinder extends Binder {
        public AddAgentToMap getServerInstance() {
            return AddAgentToMap.this;
        }
    }
}

