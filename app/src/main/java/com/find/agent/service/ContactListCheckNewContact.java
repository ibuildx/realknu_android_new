package com.find.agent.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.find.agent.activity.AgentActivity;
import com.find.agent.adapter.AdapterForAgentCard;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * Created by leaditteam on 02.06.17.
 */

public class ContactListCheckNewContact extends Service {
    IBinder LocalBinder = new ContactListCheckNewContact.LocalBinder();
    int sizePending;
    String agentId;
    
    public static final String NOTIFICATION = "com.find.agent.service.ContactListCheckNewContact";
    
    @Override
    public void onCreate() {
        super.onCreate();
    }
    
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(agentId!=null) {
            if (!agentId.equals(""))
            loadRelation();
        }
        return super.onStartCommand(intent, flags, startId);
    }
    
    public void loadRelation() {
        /*
        final LoadRelationsQueryBuilder<Map<String, Object>> loadRelationsQueryBuilder = LoadRelationsQueryBuilder.ofMap();
        loadRelationsQueryBuilder.setRelationName("pending");
        Backendless.Data.of(BackendlessUser.class).loadRelations(agentId, loadRelationsQueryBuilder,
                new AsyncCallback<List<Map<String, Object>>>() {
                    @Override
                    public void handleResponse(List<Map<String, Object>> maps) {
                        if (sizePending < maps.size()){
                            sizePending = maps.size();
                            sendResultToActivity(RESULT_OK);
                        }else if (sizePending >=maps.size()){
                            sizePending = maps.size();
                            sendResultToActivity(RESULT_CANCELED);
                        }
                    }
                    
                    @Override
                    public void handleFault(BackendlessFault backendlessFault) {
                        
                    }
                });
                */
        
    }
    private void sendResultToActivity(int RESULT){
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra("result", RESULT);
        sendBroadcast(intent);
    }
    
    public void setData(int sizePending, String agentId) {
        this.sizePending = sizePending;
        this.agentId = agentId;
    }
    
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return LocalBinder;
    }
    /**
     * Local classes helper
     */
    public class LocalBinder extends Binder {
        public ContactListCheckNewContact getServerInstance() {
            return ContactListCheckNewContact.this;
        }
    }
}
