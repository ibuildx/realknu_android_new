package com.find.agent.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.find.agent.R;
import com.find.agent.fonts.CustomFontButton;
import com.find.agent.utility.BackendSettings;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.find.agent.activity.CustomerMapsActivity.CUSTOMER_PREFERENCES;
import static com.find.agent.activity.CustomerMapsActivity.EmailCustomer;
import static com.find.agent.activity.CustomerMapsActivity.FirstNameCustomer;
import static com.find.agent.activity.CustomerMapsActivity.LastNameCustomer;
import static com.find.agent.activity.CustomerMapsActivity.PASSWORD;
import static com.find.agent.activity.CustomerMapsActivity.PhoneCustomer;
import static com.find.agent.activity.CustomerRegistrationEmailActivity.EMAIL;
import static com.find.agent.activity.CustomerRegistrationEmailActivity.PHONE;
import static com.find.agent.activity.LoginActivity.FACEBOOK_LOGIN_AND_REGISTRATION;
import static com.find.agent.activity.LoginActivity.REQUEST_COARSE_LOCATION;


/**
 * Created by iBuildX on 1/12/2017.
 */

public class CustomerRegistrationNameActivity extends AppCompatActivity {

    public static String FIRST_NAME = "firstName";
    public static String LAST_NAME = "LastName";
    public static String CUSTOMER_REGISTRATION_VIA_FACEBOOK = "customerRegistrationVieaFaceboock";

    @BindView(R.id.customer_reg_first_name_editText)
    EditText firstNameEditText;
    @BindView(R.id.customer_reg_last_name_editText)
    EditText lastNameEditText;
    @BindView(R.id.login_buttonFB)
    LoginButton login_buttonFB;
    @BindView(R.id.loginImageFb)
    CustomFontButton loginImageFb;
    
    CallbackManager callbackManager;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_registration_name);
        ButterKnife.bind(this);
    
        SharedPreferences sharedPreferencesCustomer = getSharedPreferences(CUSTOMER_PREFERENCES, Context.MODE_PRIVATE);
        if ((sharedPreferencesCustomer.contains(FirstNameCustomer)) &&(sharedPreferencesCustomer.contains(LastNameCustomer))){
            firstNameEditText.setText(sharedPreferencesCustomer.getString(FirstNameCustomer,""));
            lastNameEditText.setText(sharedPreferencesCustomer.getString(LastNameCustomer,""));
        }
        FACEBOOK_LOGIN_AND_REGISTRATION = false;
        facebookLoginAndRegistration();
    }
    
    private void facebookLoginAndRegistration() {
        login_buttonFB.setReadPermissions(Arrays.asList(
                "public_profile", "email", "user_birthday", "user_friends"));
        callbackManager = CallbackManager.Factory.create();
        
        // Callback registration
        login_buttonFB.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginActivity", response.toString());
                                    try {
                                        String email = object.getString("email");
                                        String name = object.getString("name");
                                        String id = object.getString("id");
                                        
                                        String[] names = name.split(" ");
    
                                        Intent mapIntent = new Intent(CustomerRegistrationNameActivity.this, CustomerMapsActivity.class);
                                        mapIntent.putExtra(FIRST_NAME, names[0]);
                                        mapIntent.putExtra(LAST_NAME, names[1]);
                                        mapIntent.putExtra(EMAIL, email);
                                        mapIntent.putExtra(PHONE, CUSTOMER_REGISTRATION_VIA_FACEBOOK);
                                        mapIntent.putExtra(PASSWORD,id);
                                        startActivity(mapIntent);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                LoginManager.getInstance().logOut();
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email");
                request.setParameters(parameters);
                request.executeAsync();
            }
            
            @Override
            public void onCancel() {
              
            }
            
            @Override
            public void onError(FacebookException exception) {
              
            }
        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
    
    @OnClick(R.id.loginImageFb)
    void clickFb(){
        FACEBOOK_LOGIN_AND_REGISTRATION = true;
        if(BackendSettings.isNetworkConnected(this)) {
            login_buttonFB.callOnClick();
        }
    }
    
    @OnClick(R.id.reg_name_next_button)
    void nameNext(){
        goToNextActivity();
    }
    
    @OnClick(R.id.reg_name_next_button_arrow)
    void nameNextArrow(){
        goToNextActivity();
    }
    
    private void goToNextActivity(){
        FACEBOOK_LOGIN_AND_REGISTRATION = false;
        if (firstNameEditText.getText().toString().isEmpty()){
            Toast.makeText(this, "First name is empty.",Toast.LENGTH_SHORT).show();
            return;
        }
        if (lastNameEditText.getText().toString().isEmpty()){
            Toast.makeText(this, "Last name is empty.",Toast.LENGTH_SHORT).show();
            return;
        }
        Intent nameIntent = new Intent(this, CustomerRegistrationEmailActivity.class);
        nameIntent.putExtra(FIRST_NAME, firstNameEditText.getText().toString());
        nameIntent.putExtra(LAST_NAME, lastNameEditText.getText().toString());
        startActivity(nameIntent);
        finish();
    }
    
}
