package com.find.agent.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.find.agent.R;
import com.find.agent.dialog.TermsAndConditionPopUpActivity;
import com.find.agent.fonts.CustomFontButton;
import com.find.agent.utility.BackendSettings;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by iBuildX on 10/4/2016.
 */

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.login_button)
    Button loginButton;
    @BindView(R.id.register_button)
    Button registerButton;
    @BindView(R.id.username_edittext)
    EditText usernameEditText;
    @BindView(R.id.login_password_editText)
    EditText passwordEditText;
    @BindView(R.id.email_edit_text)
    EditText emailEditText;
    @BindView(R.id.lgin_signup_indicate_view)
    View indicatorView;
    @BindView(R.id.already_agent_button)
    TextView alreadAgentButton;
    @BindView(R.id.terms_checkBox)
    CheckBox termsConditionCheckBox;
    @BindView(R.id.login_buttonFB)
    LoginButton login_buttonFB;
    @BindView(R.id.loginImageFb)
    CustomFontButton loginImageFb;
    String usernameText;
    String passwordText;
    String emailText;

    ProgressDialog progressDialog;

    public static final String AGENT_PREFERENCES = "MyPrefs";
    public static final String AGENT_PREFERENCES_FACEBOOK = "MyPrefsFB";
    public static final String Login = "Login";
    public static final String USERNAME = "email";
    public static final String Password = "password";
    public static final String ObjectId = "objectId";
    public static final String Name = "name";
    public static final String Customer = "agent";
    public static final int REQUEST_COARSE_LOCATION = 1231;

    private final int ACTIVITY_RESULT = 1212;
    SharedPreferences sharedpreferencesAgentCustom;
    SharedPreferences sharedpreferencesAgentFB;
    SharedPreferences.Editor editor;

    LocationManager mLocationManager;

    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;

    private String usernametext;
    private String passTextTemp;
    private Boolean checkBoxResult = true;
    CallbackManager callbackManager;
    public static Boolean FACEBOOK_LOGIN_AND_REGISTRATION = false;
    private Boolean registrationAndLoginFacebook = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        sharedpreferencesAgentCustom = getSharedPreferences(AGENT_PREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferencesAgentCustom.edit();

        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        usernametext = setUNAMEAgentText();
        passTextTemp = setPassAgentText();
        findViewById(R.id.login_form_show_button).performClick();
        facebookLoginAndRegistration();
        termsConditionCheckBox.setChecked(true);
        createGoogleApiClient();
    }

    public void facebookLogin(final JSONObject object) throws Exception{

        emailText = object.getString("email");
        usernametext = object.getString("name");
        passwordText = object.getString("id");
        if (!LoginActivity.this.isFinishing())
            progressDialog = ProgressDialog.show(LoginActivity.this, "", "Please wait for Login...");

        Backendless.UserService.login(usernametext, passwordText, new AsyncCallback<BackendlessUser>() {
            public void handleResponse(BackendlessUser user) {

                editor.putBoolean(Login, true);
                editor.putString(USERNAME, usernametext);
                editor.putString(Password, passwordText);
                editor.putString(ObjectId, user.getObjectId());
                editor.commit();
                if (mLastLocation != null) {

                    user.setProperty("longitude", String.valueOf(mLastLocation.getLongitude()));
                    user.setProperty("latitude", String.valueOf(mLastLocation.getLatitude()));

                    Backendless.UserService.update(user, new AsyncCallback<BackendlessUser>() {
                        @Override
                        public void handleResponse(BackendlessUser backendlessUser) {

                        }

                        @Override
                        public void handleFault(BackendlessFault backendlessFault) {

                        }

                    });
                }
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                Intent intent = new Intent(LoginActivity.this, AgentActivity.class);
                startActivity(intent);
                finish();

            }

            public void handleFault(BackendlessFault fault) {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                registerAccount(usernametext, passwordText, emailText);


            }
        });
    }

    private void facebookLoginAndRegistration() {
        login_buttonFB.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday", "user_friends"));
        callbackManager = CallbackManager.Factory.create();

        // Callback registration
        login_buttonFB.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginActivity", response.toString());

                                try {

                                    facebookLogin(object);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                                /*if (!registrationAndLoginFacebook) {
                                    try {
                                        String email = object.getString("email");
                                        String name = object.getString("name");
                                        String id = object.getString("id");
                                        loginAccount(name, id, sharedpreferencesAgentFB.edit());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    try {
                                        String email = object.getString("email");
                                        String name = object.getString("name");
                                        String id = object.getString("id");
                                        registerAccount(name, id, email);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }*/
                                LoginManager.getInstance().logOut();
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException exception) {

            }
        });
    }


    /**
     * This method get some data from editText fields and
     * just send it into next activity.
     */
    public void registerAccount(String username, String password, String email) {
        usernameText = username;
        passwordText = password;
        emailText = email;

        if (mLastLocation == null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity) this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission_group.LOCATION}, REQUEST_COARSE_LOCATION);
                return;
            } else {
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                        mGoogleApiClient);
                if (mLastLocation == null) {
                    Toast.makeText(LoginActivity.this, "Location: null", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
        if (TextUtils.isEmpty(emailText)) {
            Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!isValidEmail(emailText)) {
            Toast.makeText(getApplicationContext(), "Invalid email address!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(passwordText)) {
            Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (passwordText.length() < 6) {
            Toast.makeText(getApplicationContext(), "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!termsConditionCheckBox.isChecked()) {
            Toast.makeText(getApplicationContext(), "Please accept terms and conditions first!", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(LoginActivity.this, AgentQuestionsActivity.class);
        intent.putExtra("name", usernameText);
        intent.putExtra("email", emailText);
        intent.putExtra("pass", passwordText);
        intent.putExtra("long", String.valueOf(mLastLocation.getLatitude()));
        intent.putExtra("lat", String.valueOf(mLastLocation.getLongitude()));
        startActivityForResult(intent, ACTIVITY_RESULT);
    }

    /**
     * Login account and if we have CurLocation
     * then update it on base
     */
    public void loginAccount(final String username, String pass, final SharedPreferences.Editor editor) {
        usernametext = username;
        passwordText = pass;


        if (TextUtils.isEmpty(username)) {
            Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(passwordText)) {
            Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!LoginActivity.this.isFinishing())
            progressDialog = ProgressDialog.show(LoginActivity.this, "", "Please wait for Login...");

        Backendless.UserService.login(username, passwordText, new AsyncCallback<BackendlessUser>() {
            public void handleResponse(BackendlessUser user) {

                editor.putBoolean(Login, true);
                editor.putString(USERNAME, usernametext);
                editor.putString(Password, passwordText);
                editor.putString(ObjectId, user.getObjectId());
                editor.commit();
                if (mLastLocation != null) {

                    user.setProperty("longitude", String.valueOf(mLastLocation.getLongitude()));
                    user.setProperty("latitude", String.valueOf(mLastLocation.getLatitude()));

                    Backendless.UserService.update(user, new AsyncCallback<BackendlessUser>() {
                        @Override
                        public void handleResponse(BackendlessUser backendlessUser) {

                        }

                        @Override
                        public void handleFault(BackendlessFault backendlessFault) {

                        }

                    });
                }
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                Intent intent = new Intent(LoginActivity.this, AgentActivity.class);
                startActivity(intent);
                finish();

            }

            public void handleFault(BackendlessFault fault) {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(LoginActivity.this, "login failed: " + fault.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }

    @OnClick(R.id.login_form_show_button)
    void loginForm() {

        registrationAndLoginFacebook = false;
        indicatorView.setBackground(getResources().getDrawable(R.drawable.login_bar));
        usernameEditText.setVisibility(View.VISIBLE);
        emailEditText.setVisibility(View.GONE);
        registerButton.setVisibility(View.GONE);
        loginButton.setVisibility(View.VISIBLE);
        alreadAgentButton.setVisibility(View.GONE);
        termsConditionCheckBox.setVisibility(View.GONE);

        usernametext = setUNAMEAgentText();
        passTextTemp = setPassAgentText();
        if (!usernametext.equals("null")) usernameEditText.setText(usernametext);
        if (!passTextTemp.equals("null")) passwordEditText.setText(passTextTemp);
    }

    @OnClick(R.id.signup_form_show_button)
    void signUpForm() {
        registrationAndLoginFacebook = true;
        indicatorView.setBackground(getResources().getDrawable(R.drawable.signup_bar));
        usernameEditText.setVisibility(View.VISIBLE);
        emailEditText.setVisibility(View.VISIBLE);
        registerButton.setVisibility(View.VISIBLE);
        loginButton.setVisibility(View.GONE);
        alreadAgentButton.setVisibility(View.VISIBLE);
        termsConditionCheckBox.setVisibility(View.VISIBLE);
        termsConditionCheckBox.setChecked(true);

        emailEditText.setText("");
        passwordEditText.setText("");
        usernameEditText.setText("");
    }

    private String setUNAMEAgentText() {
        String email = sharedpreferencesAgentCustom.getString(USERNAME, "null");
        if (!email.equals("null")) {
            emailEditText.setText(email);
            return email;
        } else
            return "null";
    }

    private String setPassAgentText() {
        String pass = sharedpreferencesAgentCustom.getString(Password, "null");
        if (!pass.equals("null")) {
            passwordEditText.setText(pass);
            return pass;
        } else
            return "null";
    }

    @OnClick(R.id.already_agent_button)
    void alredyAgent() {
        loginForm();
    }

    @OnClick(R.id.login_button)
    void login() {
        String emailTextTemp = usernameEditText.getText().toString();
        String passwordText = passwordEditText.getText().toString();
        sharedpreferencesAgentCustom = getSharedPreferences(AGENT_PREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferencesAgentCustom.edit();
        FACEBOOK_LOGIN_AND_REGISTRATION = false;
        if(BackendSettings.isNetworkConnected(this)) {
            loginAccount(emailTextTemp, passwordText, editor);
        }
    }

    @OnClick(R.id.loginImageFb)
    void fbClick() {
        if(BackendSettings.isNetworkConnected(this)) {
            sharedpreferencesAgentFB = getSharedPreferences(AGENT_PREFERENCES_FACEBOOK, Context.MODE_PRIVATE);
            FACEBOOK_LOGIN_AND_REGISTRATION = true;
            login_buttonFB.callOnClick();
        }
    }

    @OnClick(R.id.register_button)
    void register() {

        String usernameText = usernameEditText.getText().toString();
        String passwordText = passwordEditText.getText().toString();
        String emailText = emailEditText.getText().toString();
        FACEBOOK_LOGIN_AND_REGISTRATION = false;
        if(BackendSettings.isNetworkConnected(this)) {
            registerAccount(usernameText, passwordText, emailText);
        }
    }

    @OnClick(R.id.terms_checkBox)
    void termAndConditionsOnClick() {
        if (termsConditionCheckBox.isChecked()) {
            Intent intent = new Intent(this, TermsAndConditionPopUpActivity.class);
            startActivityForResult(intent, ACTIVITY_RESULT);
        }
    }

    private void createGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACTIVITY_RESULT) {
            if (resultCode == RESULT_OK) {
                checkBoxResult = true;
                termsConditionCheckBox.setChecked(true);
            } else {
                termsConditionCheckBox.setChecked(false);
            }
            if (resultCode == RESULT_FIRST_USER) {
                checkBoxResult = false;
            }
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    boolean isValidEmail(String email) {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z0-9]+\\.+[a-z]+";
        if (email.matches(emailPattern) && email.length() > 0) {
            return true;
        } else return false;
    }

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            mLastLocation = location;
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    @Override
    public void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        displayLocationSettingsRequest(this);
        if (!checkBoxResult) {
            if (!emailEditText.getText().toString().isEmpty()) emailEditText.setText("");
            if (!passwordEditText.getText().toString().isEmpty()) passwordEditText.setText("");
            if (!usernameEditText.getText().toString().isEmpty()) usernameEditText.setText("");
        }
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_COARSE_LOCATION) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            } else
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                        mGoogleApiClient);
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission_group.LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission_group.LOCATION}, REQUEST_COARSE_LOCATION);
            return;
        } else {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100,
                    100, mLocationListener);
        }

    }

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i("", "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i("", "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            status.startResolutionForResult(LoginActivity.this, REQUEST_COARSE_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i("", "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i("", "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
