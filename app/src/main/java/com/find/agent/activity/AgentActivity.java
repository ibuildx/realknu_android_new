package com.find.agent.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.azoft.carousellayoutmanager.CarouselLayoutManager;
import com.azoft.carousellayoutmanager.CarouselZoomPostLayoutListener;
import com.azoft.carousellayoutmanager.CenterScrollListener;
import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.backendless.persistence.QueryOptions;
import com.find.agent.MyService;
import com.find.agent.R;
import com.find.agent.adapter.AdapterForAgentCard;
import com.find.agent.drawer.BaseActivity;
import com.find.agent.model.AppHepler;
import com.find.agent.service.ContactListCheckNewContact;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.find.agent.activity.AgentQuestionsActivity.FIRST_NAME;
import static com.find.agent.activity.AgentQuestionsActivity.LAST_NAME;
import static com.find.agent.activity.LoginActivity.AGENT_PREFERENCES;
import static com.find.agent.activity.LoginActivity.AGENT_PREFERENCES_FACEBOOK;
import static com.find.agent.activity.LoginActivity.USERNAME;
import static com.find.agent.activity.LoginActivity.FACEBOOK_LOGIN_AND_REGISTRATION;
import static com.find.agent.activity.LoginActivity.ObjectId;
import static com.find.agent.activity.LoginActivity.Password;

/**
 * Created by leaditteam on 31.05.17.
 */

public class AgentActivity extends BaseActivity {
    String titleActionBar = "Contact List";
    private String[] navMenuTitles;
    SharedPreferences sharedpreferences;
    BackendlessUser backendlessUser;
    ProgressDialog progressDialog;
    String agentId;
    TextView textView;

    private Intent serviceIntent;
    private PendingIntent pendingIntent;
    private AlarmManager alarmManager;

    private AdapterForAgentCard adapterForAgentCard;

    private final long TIME_TO_WAKEUP_SERVICE = (AlarmManager.INTERVAL_FIFTEEN_MINUTES / 15) / 60;

    int sizePending;

    RelativeLayout newContactPending;

    private Boolean light = true;

    String userEmail;
    String userPass;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent);

        init();



    }

    /**
     * just init view which we use after.
     */
    private void init() {
        textView = (TextView) findViewById(R.id.textView4);
        newContactPending = (RelativeLayout) findViewById(R.id.rl_new_contact);
        newContactPending.setVisibility(View.INVISIBLE);

        if (!FACEBOOK_LOGIN_AND_REGISTRATION) {
            sharedpreferences = getSharedPreferences(AGENT_PREFERENCES, Context.MODE_PRIVATE);
        } else {
            sharedpreferences = getSharedPreferences(AGENT_PREFERENCES_FACEBOOK, Context.MODE_PRIVATE);
        }

        agentId = sharedpreferences.getString(ObjectId, "null");
        userEmail = sharedpreferences.getString(USERNAME, "null");
        userPass = sharedpreferences.getString(Password, "null");

        Backendless.UserService.findById(agentId, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser response) {
                backendlessUser = response;
                backendlessUser.setProperty("agentIsOnline", false);
                backendlessUser.setProperty("loginStatus", false? "loggedIn" : "loggedOff");

                String name = (response.getProperty(FIRST_NAME) != null ? response.getProperty(FIRST_NAME).toString() + " " : "") + (response.getProperty(LAST_NAME) != null ? response.getProperty(LAST_NAME).toString() : "");
                ((TextView)findViewById(R.id.text_agent)).setText(getString(R.string.logged_in_as_agent)+" {"+name+"}");
                loadRelation(false);
            }
            @Override
            public void handleFault(BackendlessFault backendlessFault) {
                Log.d("error handleFault 57", "done");
            }
        });


        Intent intent = new Intent(this, MyService.class);
        intent.putExtra("agentId", agentId);
        //startService(intent);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(titleActionBar);

        progressDialog = ProgressDialog.show(AgentActivity.this, "", "Please wait...");

        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items); // load titles from strings.xml
        set(navMenuTitles);
    }

    /**
     * Get list of pending users from Backendless
     */
    int containsSameObj = 0;
    boolean isFirstTime = true;
    int pageSize = 100, pageOffset = 0;
    List<BackendlessUser> agentList;

    public void loadRelation(final boolean ifRefresh) {

        StringBuilder whereClause = new StringBuilder();
        whereClause.append("status = 'pending'");
        whereClause.append(" and ");
        whereClause.append("receiver.objectId = '" + agentId + "'");

        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setWhereClause(whereClause.toString());
        query.setPageSize(pageSize);
        query.setOffset(pageOffset);
        QueryOptions queryOptions = new QueryOptions();
        queryOptions.addRelated("sender");
        queryOptions.addRelated("receiver");
        query.setQueryOptions(queryOptions);
//        final CountDownLatch latch = new CountDownLatch( 10 );



        final AsyncCallback<BackendlessCollection<Map>> callback = new AsyncCallback<BackendlessCollection<Map>>() {
            @Override
            public void handleResponse(BackendlessCollection<Map> mapBackendlessCollection) {
                Log.e("Result : ", mapBackendlessCollection.getData().size() + "");
                Log.e("Result : ", mapBackendlessCollection.getTotalObjects() + "");

                if (isFirstTime) {
                    isFirstTime = false;
                    agentList = new LinkedList<>();
                }

                if (agentList.size() < mapBackendlessCollection.getTotalObjects()) {

                    AppHepler parseMapToBackendlessUser = new AppHepler();

                    for (int i = 0; i < mapBackendlessCollection.getData().size(); i++) {
                        agentList.add(parseMapToBackendlessUser.parseMapToBackendlessUser(mapBackendlessCollection.getData(), i));
                    }

                    adapterForAgentCard = new AdapterForAgentCard(agentList, AgentActivity.this, backendlessUser);
                    adapterForAgentCard.notifyDataSetChanged();

                    setUpCardItems(adapterForAgentCard);

                    pageOffset = pageOffset + pageSize;
//                    mapBackendlessCollection.getPage(pageSize, pageOffset, this);//nextPage(this);
//                      mapBackendlessCollection.nextPage( this );

                    if (!ifRefresh) {
                        createServiceWhoCheckNewPending(mapBackendlessCollection.getData().size());
                    }
                }
            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {
                Log.e("Result : ", backendlessFault.getMessage());
            }
        };

        Backendless.Data.of("Inbox").find(query, callback);
//        try {
//            latch.await();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

       /* final LoadRelationsQueryBuilder<Map<String, Object>> loadRelationsQueryBuilder = LoadRelationsQueryBuilder.ofMap();
        loadRelationsQueryBuilder.setRelationName("pending");
        Backendless.Data.of(BackendlessUser.class).loadRelations(agentId, loadRelationsQueryBuilder,
                new AsyncCallback<List<Map<String, Object>>>() {
                    @Override
                    public void handleResponse(List<Map<String, Object>> maps) {
                        List<BackendlessUser> response = new LinkedList<BackendlessUser>();
                        AppHepler parseMapToBackendlessUser = new AppHepler();
                        
                        for (int i = 0; i < maps.size(); i++) {
                            response.add(parseMapToBackendlessUser.parseMapToBackendlessUser(maps, i));
                        }
                        
                        loadRelationsQueryBuilder.prepareNextPage();
                        
                        adapterForAgentCard = new AdapterForAgentCard(response, AgentActivity.this, agentId);
                        adapterForAgentCard.notifyDataSetChanged();
                        
                        setUpCardItems(adapterForAgentCard);
                        
                        if (!ifRefresh) {
                            createServiceWhoCheckNewPending(maps.size());
                          
                        }
                    }
                    
                    @Override
                    public void handleFault(BackendlessFault backendlessFault) {
                        progressDiaLog.eismiss();
                    }
                });
     */
    }

    /**
     * When we have list of Backendless users we need add it to view.
     *
     * @param adapterForAgentCard
     */
    private void setUpCardItems(AdapterForAgentCard adapterForAgentCard) {
        // vertical and cycle layout
        final CarouselLayoutManager layoutManager = new CarouselLayoutManager(CarouselLayoutManager.HORIZONTAL, false);
        layoutManager.setPostLayoutListener(new CarouselZoomPostLayoutListener());
        layoutManager.setMaxVisibleItems(100);

        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recicleView_agent);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapterForAgentCard);
        recyclerView.addOnScrollListener(new CenterScrollListener());
        setTextPadding(recyclerView);

    }

    /**
     * Set text padding "Pop up menu here"
     *
     * @param recyclerView
     */
    private void setTextPadding(final RecyclerView recyclerView) {
        recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                try {
                    final View v = recyclerView.getChildAt(0).findViewById(R.id.root_pop);
                    v.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    textView.setPadding(0, 0, 0, v.getHeight() + (v.getHeight() / 4));
                } catch (Exception e) {
                    DisplayMetrics metrics = AgentActivity.this.getResources().getDisplayMetrics();
                    textView.setPadding(0, 0, 0, metrics.heightPixels / 3);
                }
            }

            ;
        });
    }

    @Override
    protected void onDestroy() {
        Log.d("onDestory", "called");


        super.onDestroy();
    }


    @Override
    protected void onStop() {
        setOnlineMode(false);
        Log.d("onStop", "called");
        super.onStop();
    }

    private void setOnlineMode(final Boolean mode) {
        Backendless.UserService.findById(agentId, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser response) {
                if (light) {
                    setIntentData(response.getObjectId(), false);
                    light = false;
                }

                response.setProperty("agentIsOnline", mode);
                response.setProperty("loginStatus", mode? "loggedIn" : "loggedOff");

                Backendless.UserService.update(response, new AsyncCallback<BackendlessUser>() {
                    @Override
                    public void handleResponse(BackendlessUser backendlessUser) {
                        if(!isFinishing())
                            progressDialog.dismiss();
                    }

                    @Override
                    public void handleFault(BackendlessFault backendlessFault) {

                    }
                });
            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {

            }
        });
    }

    /**
     * this service is checking connection about new pending users. If we have new pending user then we need show notification card
     *
     * @param sizePending
     */
    private void createServiceWhoCheckNewPending(int sizePending) {
        this.sizePending = sizePending;
        stopService(getIntent());
        serviceIntent = new Intent(this, ContactListCheckNewContact.class);
        bindService(serviceIntent, mServerConn, BIND_AUTO_CREATE);
        pendingIntent = PendingIntent.getService(this, 0, serviceIntent, 0);
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, 1, TIME_TO_WAKEUP_SERVICE, pendingIntent);
    }

    protected ServiceConnection mServerConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            ContactListCheckNewContact.LocalBinder mLocalBinder = (ContactListCheckNewContact.LocalBinder) binder;
            mLocalBinder.getServerInstance().setData(sizePending, agentId);
            Log.e("Log", "onServiceConnected");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.e("Log", "onServiceDisconnected");
        }
    };
    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {

                int resultCode = bundle.getInt("result");
                if (resultCode == RESULT_OK) {
                    newContactPending.setVisibility(View.VISIBLE);
                    CardView cardOk = (CardView) findViewById(R.id.card_ok);
                    cardOk.setOnClickListener(cardOkClick);
                    loadRelation(true);
                } else if (resultCode == RESULT_CANCELED) {
                    loadRelation(true);
                }
            }
        }
    };

    View.OnClickListener cardOkClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            newContactPending.setVisibility(View.INVISIBLE);
        }
    };


    @Override
    protected void onPause() {
        if ((serviceIntent != null) && (alarmManager != null)) {
            alarmManager.cancel(pendingIntent);
        }
        unregisterReceiver(receiver);
        setOnlineMode(false);
        super.onPause();
    }

    @Override
    protected void onPostResume() {
        if ((serviceIntent != null) && (alarmManager != null)) {
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, 1, TIME_TO_WAKEUP_SERVICE, pendingIntent);
        }
        registerReceiver(receiver, new IntentFilter(
                ContactListCheckNewContact.NOTIFICATION));
        setOnlineMode(true);
        super.onPostResume();
    }



    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AgentActivity.this);
        builder.setTitle("Log out?");
        builder.setMessage("Are you sure, you want to Logout?");
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AgentActivity.this.finish();
                unbindService();
            }
        }).show();
    }

    private void unbindService() {
        if ((serviceIntent != null) && (mServerConn != null)) {
            alarmManager.cancel(pendingIntent);
            unbindService(mServerConn);
        }
    }
}
