package com.find.agent.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.find.agent.R;

import static com.find.agent.activity.AgentQuestionsActivity.COMPANY_NAME;
import static com.find.agent.activity.AgentQuestionsActivity.LANGUAGE;
import static com.find.agent.activity.AgentQuestionsActivity.LICENSE_NUMBER;
import static com.find.agent.activity.AgentQuestionsActivity.PHONE_NUMBER;
import static com.find.agent.activity.AgentQuestionsActivity.SPECIALITY;
import static com.find.agent.activity.LoginActivity.AGENT_PREFERENCES;


/**
 * Created by iBuildX on 1/11/2017.
 */

public class ApprovalRegistrationActivity extends AppCompatActivity {
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approval_registration);
    
        sharedpreferences = getSharedPreferences(AGENT_PREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        clearSharedPreference();
    }
    private void clearSharedPreference(){
        editor.remove(COMPANY_NAME);
        editor.remove(LICENSE_NUMBER);
        editor.remove(PHONE_NUMBER);
        editor.remove(LANGUAGE);
        editor.remove(SPECIALITY);
        editor.remove("firstName");
        editor.remove("lastLame");
        editor.commit();
    }
}
