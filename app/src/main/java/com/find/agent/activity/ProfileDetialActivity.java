package com.find.agent.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.files.BackendlessFile;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.find.agent.R;
import com.find.agent.model.AppHepler;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.find.agent.activity.AgentQuestionsActivity.COMPANY_NAME;
import static com.find.agent.activity.AgentQuestionsActivity.FIRST_NAME;
import static com.find.agent.activity.AgentQuestionsActivity.LANGUAGE;
import static com.find.agent.activity.AgentQuestionsActivity.LAST_NAME;
import static com.find.agent.activity.AgentQuestionsActivity.LICENSE_NUMBER;
import static com.find.agent.activity.AgentQuestionsActivity.PHONE_NUMBER;
import static com.find.agent.activity.CustomerMapsActivity.CUSTOMER_PREFERENCES;
import static com.find.agent.activity.CustomerMapsActivity.EmailCustomer;
import static com.find.agent.activity.CustomerMapsActivity.FirstNameCustomer;
import static com.find.agent.activity.CustomerMapsActivity.LastNameCustomer;
import static com.find.agent.activity.CustomerMapsActivity.PhoneCustomer;
import static com.find.agent.activity.LoginActivity.AGENT_PREFERENCES;
import static com.find.agent.utility.BackendSettings.API_TO_BACKENDLESS_IMAGE_STRING;
import static com.find.agent.utility.BackendSettings.APPLICATION_ID;
import static com.find.agent.utility.BackendSettings.SERVER_URL;
import static com.find.agent.utility.BackendSettings.VERSION_NAME_BACKENDLESS;

/**
 * Created by iBuildX on 1/24/2017.
 */

public class ProfileDetialActivity extends AppCompatActivity {


    @BindView(R.id.profile_email_textview)
    EditText emailEditText;
    @BindView(R.id.profile_first_name_textview)
    EditText fistNameEditText;
    @BindView(R.id.profile_last_name_textview)
    EditText lastNameEditText;
    @BindView(R.id.profile_full_name_textview)
    TextView fullNameTextview;
    @BindView(R.id.profile_phone_textview)
    EditText phoneEditText;
    @BindView(R.id.company_phone_textview)
    EditText companyEditText;
    @BindView(R.id.license_phone_textview)
    EditText licenseEditText;
//    @BindView(R.id.speciality_phone_textview)
//    EditText specialityEditText;
    @BindView(R.id.language_phone_textview)
    EditText languageEditText;
    @BindView(R.id.list_item_user_imageview)
    CircleImageView profileImage;
    @BindView(R.id.constrain_layout_profile)
    ConstraintLayout rootView;
    
    SharedPreferences sharedPreferencesCustomer;
    SharedPreferences.Editor editor;
    String objectId;
    Boolean ifCustomer;
    ProgressDialog progress;
    String urlToImage;
    private static final int FILE_SELECT_CODE = 0;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_detail);
        ButterKnife.bind(this);
        
        progress = new ProgressDialog(this);
        progress.setMessage("Wait while we load data...");
        progress.show();
        
        objectId = getIntent().getStringExtra("obj");
        ifCustomer = getIntent().getBooleanExtra("ifcustomer", true);
        
        checkIfItsCustomer();
        setDataFromDatabaseAboutUser();
        downloadPhotoAndUpdate();
        
    }
    
    private void checkIfItsCustomer() {
        if (ifCustomer) {
            sharedPreferencesCustomer = getSharedPreferences(CUSTOMER_PREFERENCES, Context.MODE_PRIVATE);

            ((LinearLayout) findViewById(R.id.linearLayout6)).setVisibility(View.INVISIBLE);
            ((LinearLayout) findViewById(R.id.linearLayout7)).setVisibility(View.INVISIBLE);
            ((LinearLayout) findViewById(R.id.linearLayout8)).setVisibility(View.INVISIBLE);
            ((LinearLayout) findViewById(R.id.linearLayout9)).setVisibility(View.INVISIBLE);
        } else {
            sharedPreferencesCustomer = getSharedPreferences(AGENT_PREFERENCES, Context.MODE_PRIVATE);
        }
    }
    
    private void setDataFromDatabaseAboutUser() {
        Backendless.UserService.findById(objectId, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser response) {
                
                emailEditText.setText(response.getEmail());
                fistNameEditText.setText(response.getProperty(FIRST_NAME)!= null ? response.getProperty(FIRST_NAME).toString() : "");
                lastNameEditText.setText(response.getProperty(LAST_NAME) != null ? response.getProperty(LAST_NAME).toString() : "");
                fullNameTextview.setText((response.getProperty(FIRST_NAME) != null ? response.getProperty(FIRST_NAME).toString() + " " : "") + (response.getProperty(LAST_NAME) != null ? response.getProperty(LAST_NAME).toString() : ""));
                phoneEditText.setText(response.getProperty(PHONE_NUMBER) != null ? response.getProperty(PHONE_NUMBER).toString() : "");
                
                if (!ifCustomer) {
                    companyEditText.setText(response.getProperty(COMPANY_NAME) != null ? response.getProperty(COMPANY_NAME).toString() : "");
                    licenseEditText.setText(response.getProperty(LICENSE_NUMBER) != null ?response.getProperty(LICENSE_NUMBER).toString() : "");
//                    specialityEditText.setText(response.getProperty("speciality") != null ? response.getProperty("speciality").toString() : "");
                    languageEditText.setText(response.getProperty(LANGUAGE) != null ? response.getProperty(LANGUAGE).toString() : "");
                }
                
                setUpActionBar(fistNameEditText.getEditableText().toString());
                progress.dismiss();
            }
            
            @Override
            public void handleFault(BackendlessFault fault) {
                Toast.makeText(ProfileDetialActivity.this, "Backendless Eroor:   " + fault.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        
    }
    
    public void updateOnBackendlessUserInformation() {
        progress.setMessage("Update in process...");
        progress.setCancelable(true);
        progress.show();
        
        Backendless.UserService.findById(objectId, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser response) {
                
                response.setEmail(emailEditText.getEditableText().toString());
                response.setProperty(FIRST_NAME, fistNameEditText.getEditableText().toString());
                response.setProperty(LAST_NAME, lastNameEditText.getEditableText().toString());
                response.setProperty(PHONE_NUMBER, phoneEditText.getEditableText().toString());
                
                if (!ifCustomer) {
                    response.setProperty(COMPANY_NAME, companyEditText.getEditableText().toString());
                    response.setProperty(LICENSE_NUMBER, licenseEditText.getEditableText().toString());
//                    response.setProperty("speciality", specialityEditText.getEditableText().toString());
                    response.setProperty(LANGUAGE, languageEditText.getEditableText().toString());
                }
                
                Backendless.UserService.update(response, new AsyncCallback<BackendlessUser>() {
                    @Override
                    public void handleResponse(BackendlessUser response) {
                        Toast.makeText(ProfileDetialActivity.this, "Profile information Update Successfully", Toast.LENGTH_LONG).show();

                        editor = sharedPreferencesCustomer.edit();
                        editor.putString(FirstNameCustomer, fistNameEditText.getEditableText().toString());
                        editor.putString(LastNameCustomer, lastNameEditText.getEditableText().toString());
                        editor.putString(EmailCustomer, emailEditText.getEditableText().toString());
                        editor.putString(PhoneCustomer, phoneEditText.getEditableText().toString());
                        editor.commit();
                        
                        progress.dismiss();
                    }
                    
                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Toast.makeText(ProfileDetialActivity.this, "Backendless Eroor:   " + fault.getMessage(), Toast.LENGTH_LONG).show();
                        progress.dismiss();
                    }
                });
            }
            
            @Override
            public void handleFault(BackendlessFault fault) {
                progress.dismiss();
                Toast.makeText(ProfileDetialActivity.this, "Backendless Eroor:   " + fault.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
    
    /**
     * First we need the url of photo profile(example below). And in the second,
     * we need check if we have this image in database,
     * after that can try to upload.
     * @URL_EXAMPLE: https://api.backendless.com/518D3C9F-15FA-F8E4-FF18-9A118C210400/C0BD7711-11BF-F223-FFBB-121F70CFB300/files/usersPhoto/013ED9A4-6B18-1E76-FFD6-034A4DA85500.png
     */
    private int iterationError;
    private void downloadPhotoAndUpdate() {
//        urlToImage = SERVER_URL + "/" +
//                APPLICATION_ID + "/" +
//                API_TO_BACKENDLESS_IMAGE_STRING +
//                objectId + ".png";

        urlToImage = SERVER_URL + "/" +
                APPLICATION_ID + "/" +
                VERSION_NAME_BACKENDLESS  +
                API_TO_BACKENDLESS_IMAGE_STRING +
                objectId + ".png";
        Backendless.Files.exists("usersPhoto/"+objectId + ".png", new AsyncCallback<Boolean>() {
            @Override
            public void handleResponse(Boolean aBoolean) {
                if(aBoolean)
                Glide.with(getApplicationContext())
                        .load(urlToImage)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .error(getResources().getDrawable(R.drawable.profilmage))
                        .into(profileImage);
                else{
                    if(iterationError<10) {
                        iterationError++;
                        downloadPhotoAndUpdate();
                    } else iterationError =0;
                }
            }
    
            @Override
            public void handleFault(BackendlessFault backendlessFault) {
        
            }
        });
        
    }
    
    private void updatePhotoInDatabase(final Bitmap photo) {
        Backendless.Files.Android.upload(photo, Bitmap.CompressFormat.PNG, 100, objectId + ".png", "usersPhoto", new AsyncCallback<BackendlessFile>() {
            @Override
            public void handleResponse(BackendlessFile backendlessFile) {
                Snackbar.make(rootView, "Upload success.", Snackbar.LENGTH_SHORT).show();
                downloadPhotoAndUpdate();
            }
            
            @Override
            public void handleFault(BackendlessFault backendlessFault) {
                Backendless.Files.remove("usersPhoto/" + objectId + ".png", new AsyncCallback<Void>() {
                    @Override
                    public void handleResponse(Void aVoid) {
                        updatePhotoInDatabase(photo);
                    }
                    
                    @Override
                    public void handleFault(BackendlessFault backendlessFault) {
                        Toast.makeText(getApplicationContext(), "Upload failure " + backendlessFault.getMessage(), Toast.LENGTH_SHORT).show();
                        Snackbar.make(rootView, "Are you wanna try upload agin?", Snackbar.LENGTH_LONG)
                                .setAction("YES", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        updatePhotoInDatabase(photo);
                                    }
                                }).setActionTextColor(Color.RED).show();
                    }
                });
            }
        });
    }
    
    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        
        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }
    
    @OnClick(R.id.profile_edit_first_name_image_button)
    void editFirstName() {
        fistNameEditText.requestFocus();
        fistNameEditText.setEnabled(true);
        fistNameEditText.setFocusable(true);
        fistNameEditText.setCursorVisible(true);
        fistNameEditText.setBackgroundColor(Color.WHITE);
        fistNameEditText.setSelection(fistNameEditText.getText().length());
    }
    
    @OnClick(R.id.profile_edit_last_name_image_button)
    void editLastName() {
        lastNameEditText.requestFocus();
        lastNameEditText.setEnabled(true);
        lastNameEditText.setFocusable(true);
        lastNameEditText.setCursorVisible(true);
        lastNameEditText.setBackgroundColor(Color.WHITE);
        lastNameEditText.setSelection(lastNameEditText.getText().length());
        
    }
    
    @OnClick(R.id.profile_edit_email_image_button)
    void editEmail() {
        
        emailEditText.setEnabled(true);
        emailEditText.setFocusable(true);
        emailEditText.setCursorVisible(true);
        emailEditText.setBackgroundColor(Color.WHITE);
        emailEditText.requestFocus();
        emailEditText.setSelection(emailEditText.getText().length());
        
    }
    
    @OnClick(R.id.company_edit_phone_image_button)
    void companyPhone() {
        companyEditText.setEnabled(true);
        companyEditText.setFocusable(true);
        companyEditText.setCursorVisible(true);
        companyEditText.setBackgroundColor(Color.WHITE);
        companyEditText.requestFocus();
        companyEditText.setSelection(companyEditText.getText().length());
    }
    
    @OnClick(R.id.license_edit_phone_image_button)
    void licensePhone() {
        licenseEditText.setEnabled(true);
        licenseEditText.setFocusable(true);
        licenseEditText.setCursorVisible(true);
        licenseEditText.setBackgroundColor(Color.WHITE);
        licenseEditText.requestFocus();
        licenseEditText.setSelection(licenseEditText.getText().length());
    }
    
//    @OnClick(R.id.speciality_edit_phone_image_button)
//    void specialityPhone() {
//        specialityEditText.setEnabled(true);
//        specialityEditText.setFocusable(true);
//        specialityEditText.setCursorVisible(true);
//        specialityEditText.setBackgroundColor(Color.WHITE);
//        specialityEditText.requestFocus();
//        specialityEditText.setSelection(specialityEditText.getText().length());
//    }
    
    @OnClick(R.id.language_edit_phone_image_button)
    void languagePhone() {
        languageEditText.setEnabled(true);
        languageEditText.setFocusable(true);
        languageEditText.setCursorVisible(true);
        languageEditText.setBackgroundColor(Color.WHITE);
        languageEditText.requestFocus();
        languageEditText.setSelection(languageEditText.getText().length());
    }
    
    @OnClick(R.id.profile_edit_phone_image_button)
    void editPhone() {
        phoneEditText.setEnabled(true);
        phoneEditText.setFocusable(true);
        phoneEditText.setCursorVisible(true);
        phoneEditText.setBackgroundColor(Color.WHITE);
        phoneEditText.requestFocus();
        phoneEditText.setSelection(phoneEditText.getText().length());
    }
    
    @OnClick(R.id.list_item_user_imageview)
    void imageProfileClick() {
        showFileChooser();
    }
    
    @OnClick(R.id.bt_save_profile)
    void obClick() {
        updateOnBackendlessUserInformation();
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
    
    private void setUpActionBar(String title) {
        // Update the action bar title with the TypefaceSpan instance
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(title);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    
                    try {
                        Uri uri = data.getData();
                        AppHepler parseHelper = new AppHepler();
                        updatePhotoInDatabase(parseHelper.getThumbnail(uri,this));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
