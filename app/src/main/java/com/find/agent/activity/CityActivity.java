package com.find.agent.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.find.agent.R;
import com.find.agent.adapter.CustomizedSpinnerAdapter;
import com.find.agent.fonts.CustomFontEditText;
import com.find.agent.model.CityModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by iBuildX on 2/3/2017.
 */

public class CityActivity extends AppCompatActivity {
    private ListView mListview;
    private ArrayList<CityModel> cityModels = new ArrayList<>();
    CustomFontEditText searchEditText;
    CustomizedSpinnerAdapter spinnerAdapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);
    
        // Update the action bar title with the TypefaceSpan instance
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Change City");
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);
        searchEditText = (CustomFontEditText) findViewById(R.id.search);
        // Capture Text in EditText



        mListview = (ListView) findViewById(R.id.listview);
        try {
            JSONArray placesObject = new JSONArray(readAssetsFile());
            cityModels = new Gson().fromJson(placesObject.toString(), new TypeToken<ArrayList<CityModel>>() {
            }.getType());
            spinnerAdapter= new CustomizedSpinnerAdapter(this, android.R.layout.simple_spinner_item, cityModels);
            mListview.setAdapter(spinnerAdapter);
            mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent();
                    intent.putExtra("lat", cityModels.get(position).getLat());
                    intent.putExtra("lng", cityModels.get(position).getLng());
                    intent.putExtra("name", cityModels.get(position).getName());
                    setResult(RESULT_OK, intent);
                    finish();
                }
            });

            searchEditText.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable arg0) {
                    // TODO Auto-generated method stub
                    String text = searchEditText.getText().toString().toLowerCase(Locale.getDefault());
                    spinnerAdapter.filter(text);
                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1,
                                              int arg2, int arg3) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                    // TODO Auto-generated method stub
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }

    }


    @Override
    public void onBackPressed() {

      super.onBackPressed();
    }

    public String readAssetsFile() throws Exception{
        StringBuilder buf=new StringBuilder();
        InputStream json=getAssets().open("cities.json");
        BufferedReader in=
                new BufferedReader(new InputStreamReader(json, "UTF-8"));
        String str;

        while ((str=in.readLine()) != null) {
            buf.append(str);
        }

        in.close();
        return buf.toString();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
    
}
