package com.find.agent.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import com.find.agent.R;
import com.find.agent.utility.BackendSettings;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.find.agent.activity.CustomerMapsActivity.CUSTOMER_PREFERENCES;
import static com.find.agent.activity.CustomerMapsActivity.EmailCustomer;
import static com.find.agent.activity.CustomerMapsActivity.FirstNameCustomer;
import static com.find.agent.activity.CustomerMapsActivity.LastNameCustomer;
import static com.find.agent.activity.CustomerMapsActivity.PASSWORD;
import static com.find.agent.activity.CustomerMapsActivity.PhoneCustomer;
import static com.find.agent.activity.LoginActivity.REQUEST_COARSE_LOCATION;


/**
 * Created by iBuildX on 1/12/2017.
 */

public class CustomerRegistrationEmailActivity extends AppCompatActivity {

    public static String FIRST_NAME = "firstName";
    public static String LAST_NAME = "LastName";
    public static String EMAIL = "email";
    public static String PHONE = "bestPhoneNo";
    public static String CUSTOMER_OBJECTID = "customerObjectId";
    
    String firstName;
    String lastName;

    @BindView(R.id.customer_reg_email_editText)
    EditText emailEditText;
    @BindView(R.id.customer_reg_phone_editText)
    EditText phoneEditText;
    
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_registration_email);
        ButterKnife.bind(this);

        firstName = getIntent().getStringExtra(FIRST_NAME);
        lastName = getIntent().getStringExtra(LAST_NAME);
    
        SharedPreferences sharedPreferencesCustomer = getSharedPreferences(CUSTOMER_PREFERENCES, Context.MODE_PRIVATE);
        if ((sharedPreferencesCustomer.contains(EmailCustomer)) &&(sharedPreferencesCustomer.contains(PhoneCustomer))){
            emailEditText.setText(sharedPreferencesCustomer.getString(EmailCustomer,""));
            phoneEditText.setText(sharedPreferencesCustomer.getString(PhoneCustomer,""));
        }
      
    }
    
    @OnClick(R.id.reg_email_next_button)
    void emailNext() {
        startNextActivity();
    }
    
    @OnClick(R.id.reg_email_next_button_arrow)
    void emailNextArrow() {
        startNextActivity();
    }
    private void startNextActivity(){

        if (emailEditText.getText().toString().isEmpty()){
            Toast.makeText(this, "Email is empty.",Toast.LENGTH_SHORT).show();
            return;
        }
        if (!isValidEmail(emailEditText.getText().toString())){
            Toast.makeText(this, "Email invalid.",Toast.LENGTH_SHORT).show();
            return;
        }
        if (phoneEditText.getText().toString().isEmpty()){
            Toast.makeText(this, "Phone is empty.",Toast.LENGTH_SHORT).show();
            return;
        }
        Intent mapIntent = new Intent(CustomerRegistrationEmailActivity.this, CustomerMapsActivity.class);
        mapIntent.putExtra(FIRST_NAME, firstName);
        mapIntent.putExtra(LAST_NAME, lastName);
        mapIntent.putExtra(EMAIL, emailEditText.getText().toString());
        mapIntent.putExtra(PHONE, phoneEditText.getText().toString());
        mapIntent.putExtra(PASSWORD,"1234567");
        if(BackendSettings.isNetworkConnected(this)) {
            startActivity(mapIntent);
        }
    }
    
    boolean isValidEmail(String email){
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (email.matches(emailPattern) && email.length() > 0)
        {
            return true;
        } else return false;
    }
    
  
    
}
