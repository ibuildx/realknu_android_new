package com.find.agent.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.crashlytics.android.Crashlytics;
import com.find.agent.R;
import com.find.agent.utility.BackendSettings;

import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;

/**
 * Created by iBuildX on 1/11/2017.
 */

public class ChooseUserActivity extends AppCompatActivity {
    
    
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String Login = "Login";
    public static final String ObjectId = "objectId";
    private static final int REQUEST_OPEN_LOCATION_SETTING = 50000;
    public final static int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 50001;
    SharedPreferences sharedpreferences;
    
    public static final String CUSTOMER_PREFERENCES = "CustomerPrefs";
    public static final String LoginCustomer = "Login";
    public static final String LastNameCustomer = "LastName";
    public static final String FirstNameCustomer = "firstName";
    public static final String EmailCustomer = "email";
    public static final String PhoneCustomer = "phone";
    public static final String ObjectIdCustomer = "objectId";
    SharedPreferences sharedPreferencesCustomer;
    
    public static String CUSTOMER_OBJECTID = "customerObjectId";
    
    boolean login = false;
    String objectId;
    
    String objectIdCustomer;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_choose_user);
        ButterKnife.bind(this);
        checkGpsPermission();
        
    }

    boolean findLocation=false;
    public void isLocationServiceEnabled(Activity context) {

        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean isGPSEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnable = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isGPSEnable && !isNetworkEnable) {
            showSettingAlert(context);

        } else {

            performNextTask();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_OPEN_LOCATION_SETTING) {
            checkGpsPermission();
        }
    }

    public void checkGpsPermission() {


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            isLocationServiceEnabled(this);

        } else {
            // Permission is missing and must be requested.
            requestLocationPermission();
        }
    }

    private void requestLocationPermission() {

        // Should we show an explanation?
        if (ActivityCompat.shouldShowRequestPermissionRationale(ChooseUserActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)) {

            ActivityCompat.requestPermissions(ChooseUserActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ChooseUserActivity.MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        } else {

            ActivityCompat.requestPermissions(ChooseUserActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ChooseUserActivity.MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }


    public void performNextTask(){
        findLocation = true;
        Backendless.setUrl(BackendSettings.SERVER_URL);
        Backendless.initApp(this, BackendSettings.APPLICATION_ID, BackendSettings.ANDROID_SECRET_KEY, BackendSettings.VERSION );


        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        sharedPreferencesCustomer = getSharedPreferences(CUSTOMER_PREFERENCES, Context.MODE_PRIVATE);

        login = sharedpreferences.getBoolean(Login, false);
        objectId = sharedpreferences.getString(ObjectId, "null");

        objectIdCustomer = sharedPreferencesCustomer.getString(ObjectIdCustomer, "null");
    }

    public void showSettingAlert(final Activity context) {
        AlertDialog.Builder alertDlg = new AlertDialog.Builder(context);
        alertDlg.setTitle(context.getString(R.string.gps_setting));
        alertDlg.setMessage(context.getString(R.string.enable_gps));
        alertDlg.setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(myIntent, REQUEST_OPEN_LOCATION_SETTING);
            }
        });
        alertDlg.show();
    }
    
    @OnClick(R.id.agent_button)
    void agentLogin() {
        loginAgent();
    }
    
    @OnClick(R.id.agent_button_arrow)
    void agentFromArrowLogin() {
        loginAgent();
    }
    
    @OnClick(R.id.customer_button)
    void customerRegistration() {
        loginCustomer();
    }
    
    @OnClick(R.id.customer_button_arrow)
    void customerRegistrationArrow() {
        loginCustomer();
    }
    
    private void loginAgent() {
        if(findLocation) {
            Intent intent = new Intent(ChooseUserActivity.this, LoginActivity.class);
            startActivity(intent);
        }
    }

    private void loginCustomer() {
        if(findLocation) {
            Intent intent = new Intent(ChooseUserActivity.this, CustomerRegistrationNameActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkGpsPermission();

            }
        }
    }
}
