package com.find.agent.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.find.agent.R;

import java.util.ArrayList;
import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.find.agent.activity.LoginActivity.AGENT_PREFERENCES_FACEBOOK;
import static com.find.agent.activity.LoginActivity.Customer;
import static com.find.agent.activity.LoginActivity.USERNAME;
import static com.find.agent.activity.LoginActivity.FACEBOOK_LOGIN_AND_REGISTRATION;
import static com.find.agent.activity.LoginActivity.Login;
import static com.find.agent.activity.LoginActivity.AGENT_PREFERENCES;
import static com.find.agent.activity.LoginActivity.ObjectId;
import static com.find.agent.activity.LoginActivity.Password;

/**
 * Created by leaditteam on 24.05.17.
 */

public class AgentQuestionsActivity extends Activity {
//    public static final String FIRST_AND_LASTNAME = "firstandlastname";
//    public static final String COMPANY_NAME = "companyName";
//    public static final String LICENSE_NUMBER = "licenseNumber";
//    public static final String PHONE_NUMBER = "phone";
//    public static final String LANGUAGE = "language";
//    public static final String SPECIALITY = "speciality";


    public static final String FIRST_AND_LASTNAME = "QfistAndLastName";
    public static final String COMPANY_NAME = "companyName";
    public static final String LICENSE_NUMBER = "licenceNumber";
    public static final String PHONE_NUMBER = "bestPhoneNo";
    public static final String LANGUAGE = "Lenguage";
    public static final String SPECIALITY = "speciality";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "LastName";

    ProgressDialog progressDialog;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;

    private String usernameText;
    private String emailText;
    private String passwordText;
    private String latitude;
    private String longitude;
    private String firstAndLastName;
    private String companyName;
    private String licenseNumber;
    private String phoneNumber;
//    private String language;
//    private String speciality;

    @BindView(R.id.ed_question_1)
    EditText edFirstAndLastName;
    @BindView(R.id.ed_question_2)
    EditText edCompanyName;
    @BindView(R.id.ed_question_3)
    EditText edLicenseNumber;
    @BindView(R.id.ed_question_4)
    EditText edPhoneNumber;
//    @BindView(R.id.ed_question_5)
//    EditText edLanguage;
//    @BindView(R.id.ed_question_6)
//    EditText edSpeciality;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_questions);
        if (!FACEBOOK_LOGIN_AND_REGISTRATION) {
            sharedpreferences = getSharedPreferences(AGENT_PREFERENCES, Context.MODE_PRIVATE);
        } else {
            sharedpreferences = getSharedPreferences(AGENT_PREFERENCES_FACEBOOK, Context.MODE_PRIVATE);
        }
        editor = sharedpreferences.edit();
        ButterKnife.bind(this);
        getExtras();
        setUpEditTextFields();

    }

    private void getExtras() {
        Intent intent = getIntent();
        usernameText = intent.getStringExtra("name");
        emailText = intent.getStringExtra("email");
        passwordText = intent.getStringExtra("pass");
        latitude = intent.getStringExtra("long");
        longitude = intent.getStringExtra("lat");
    }

    private void setUpEditTextFields() {
        String firstName = sharedpreferences.getString("firstName", "1>");
        if (firstName.contains("1>")) {
            edFirstAndLastName.setText(usernameText);
        } else {
            edFirstAndLastName.setText(firstName);
            edCompanyName.setText(sharedpreferences.getString(COMPANY_NAME, ""));
            edLicenseNumber.setText(sharedpreferences.getString(LICENSE_NUMBER, ""));
            edPhoneNumber.setText(sharedpreferences.getString(PHONE_NUMBER, ""));
//            edLanguage.setText(sharedpreferences.getString(LANGUAGE, ""));
//            edSpeciality.setText(sharedpreferences.getString(SPECIALITY, ""));
        }
    }

    private void registerUser() {
        if (!firstAndLastName.contains(" ")) {
            Toast.makeText(this, "Please write current First&Last name.", Toast.LENGTH_SHORT).show();
            return;
        }
        final String languageString = getLanguage();
        if (languageString.equalsIgnoreCase("")) {
            Toast.makeText(this, "Please choose any langauge type", Toast.LENGTH_SHORT).show();
            return;
        }

        final String agentType = getAgentType();
        if (agentType.equalsIgnoreCase("")) {
            Toast.makeText(this, "Please check agent type.", Toast.LENGTH_SHORT).show();
            return;
        }
        final String[] firstAndLastNameTemp = firstAndLastName.split("\\s+");
        final BackendlessUser user = new BackendlessUser();
        user.setProperty("name", usernameText);
        user.setEmail(emailText);
        user.setPassword(passwordText);
        user.setProperty(FIRST_AND_LASTNAME, firstAndLastName);
        user.setProperty("latitude", latitude);
        user.setProperty("longitude", longitude);
        user.setProperty(Customer, true);
        user.setProperty(FIRST_NAME, firstAndLastNameTemp[0]);
        user.setProperty(LAST_NAME, firstAndLastNameTemp[1]);
        user.setProperty(COMPANY_NAME, companyName);
        user.setProperty(LICENSE_NUMBER, licenseNumber);
        user.setProperty(PHONE_NUMBER, phoneNumber);
        user.setProperty(LANGUAGE, languageString);
//        user.setProperty(SPECIALITY, speciality);
        user.setProperty("type", "agent");
        user.setProperty("agentType", agentType);
        user.setProperty("agentIsOnline", false);

        progressDialog = ProgressDialog.show(this, "", "Please wait for Registration...");
        Backendless.UserService.register(user, new AsyncCallback<BackendlessUser>() {
            public void handleResponse(BackendlessUser registeredUser) {
                editor.putBoolean(Login, true);
                editor.putString(USERNAME, usernameText);
                editor.putString(Password, passwordText);
                editor.putString(ObjectId, user.getObjectId());
                editor.putString("firstName", firstAndLastNameTemp[0]);
                editor.putString("LastName", firstAndLastNameTemp[1]);
                editor.commit();
                progressDialog.dismiss();
                Intent intent = new Intent(AgentQuestionsActivity.this, ApprovalRegistrationActivity.class);
                startActivity(intent);
                setResult(RESULT_FIRST_USER);
                finish();
            }

            public void handleFault(BackendlessFault fault) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "An error has occurred:\n" + fault.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }

    private String getLanguage(){
        CheckBox checkboxEnglish = (CheckBox) findViewById(R.id.checkboxEnglish);
        CheckBox checkboxFrench = (CheckBox) findViewById(R.id.checkboxFrench);
        CheckBox checkboxItalian = (CheckBox) findViewById(R.id.checkboxItalian);
        CheckBox checkboxPortuguese = (CheckBox) findViewById(R.id.checkboxPortuguese);
        CheckBox checkboxRussian = (CheckBox) findViewById(R.id.checkboxRussian);
        CheckBox checkboxSpanish = (CheckBox) findViewById(R.id.checkboxSpanish);
        String languageString = "";

        if (checkboxEnglish.isChecked()) {
            languageString += getString(R.string.txt_english)+",";
        }

        if (checkboxFrench.isChecked()) {
            languageString += getString(R.string.txt_french)+",";
        }

        if (checkboxItalian.isChecked()) {
            languageString += getString(R.string.txt_italian)+",";
        }

        if (checkboxPortuguese.isChecked()) {
            languageString += getString(R.string.txt_portuguese)+",";
        }

        if (checkboxRussian.isChecked()) {
            languageString += getString(R.string.txt_russian)+",";
        }

        if (checkboxSpanish.isChecked()) {
            languageString += getString(R.string.txt_spanish)+",";
        }

        if(languageString.endsWith(","))
            languageString = languageString.substring(0, languageString.length()-1);
        return languageString;
    }

    private String getAgentType() {
        CheckBox redAgent = (CheckBox) findViewById(R.id.redAgentCheckBox);
        CheckBox comAgent = (CheckBox) findViewById(R.id.commAgentCheckBox);
        CheckBox rentalAgent = (CheckBox) findViewById(R.id.rentalAgentCheckBox);

        String agenttype = "";
        if (redAgent.isChecked()) {
            agenttype += "RR"+",";
        }
        if (comAgent.isChecked()) {
            agenttype += "RC"+",";
        }
        if (rentalAgent.isChecked()) {
            agenttype += "Ar"+",";
        }

        if(agenttype.endsWith(","))
            agenttype = agenttype.substring(0, agenttype.length()-1);
        return agenttype;

        /*
        if (redAgent.isChecked() && comAgent.isChecked() ||
                rentalAgent.isChecked() && comAgent.isChecked() ||
                rentalAgent.isChecked() && redAgent.isChecked() ||
                rentalAgent.isChecked() && redAgent.isChecked() && comAgent.isChecked()) {
            return "RCR";
        } else if (redAgent.isChecked()) {
            return "RR";
        } else if (comAgent.isChecked()) {
            return "RC";
        } else if (rentalAgent.isChecked()) {
            return "Ar";
        } else {
            return "";
        }*/
    }



    private Boolean areWeCanRegister() {
        ArrayList<String> questionsList = createArrayQuestions();
        Iterator iterator = questionsList.listIterator();
        int i = 0;
        while (iterator.hasNext()) {
            String thisQuestion = questionsList.get(i);
            iterator.next();
            i++;
            if (TextUtils.isEmpty(thisQuestion)) {
                return false;
            }
        }
        return true;
    }

    private ArrayList<String> createArrayQuestions() {
        ArrayList<String> questionsList = new ArrayList<>();
        questionsList.add(0, edFirstAndLastName.getText().toString());
        questionsList.add(1, edCompanyName.getText().toString());
        questionsList.add(2, edLicenseNumber.getText().toString());
        questionsList.add(3, edPhoneNumber.getText().toString());
//        questionsList.add(4, edLanguage.getText().toString());
//        questionsList.add(4, edSpeciality.getText().toString());
        return questionsList;
    }

    @OnClick(R.id.select_question_btn)
    protected void selectOnClick() {
        if (!areWeCanRegister()) {
            Toast.makeText(this, "Please fill the field.", Toast.LENGTH_SHORT).show();
        } else {
            returnEditTextData();
            registerUser();
        }

    }

    private void returnEditTextData() {
        firstAndLastName = edFirstAndLastName.getText().toString();
        companyName = edCompanyName.getText().toString();
        licenseNumber = edLicenseNumber.getText().toString();
//        language = edLanguage.getText().toString();
//        speciality = edSpeciality.getText().toString();
        phoneNumber = edPhoneNumber.getText().toString();
    }

    @Override
    public void onBackPressed() {
        returnEditTextData();

        editor.putString("firstName", firstAndLastName);
        editor.putString(COMPANY_NAME, companyName);
        editor.putString(LICENSE_NUMBER, licenseNumber);
        editor.putString(PHONE_NUMBER, phoneNumber);
//        editor.putString(LANGUAGE, language);
//        editor.putString(SPECIALITY, speciality);
        editor.commit();

        setResult(RESULT_OK);
        super.onBackPressed();
    }
}
