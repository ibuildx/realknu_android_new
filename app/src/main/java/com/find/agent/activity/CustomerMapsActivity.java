package com.find.agent.activity;

import android.Manifest;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.find.agent.R;
import com.find.agent.dialog.ProgressLoadingLocationDialog;
import com.find.agent.drawer.BaseActivity;
import com.find.agent.fonts.TypefaceSpan;
import com.find.agent.service.AddAgentToMap;
import com.find.agent.tooltip.Tooltip;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


import static com.find.agent.activity.AgentQuestionsActivity.FIRST_NAME;
import static com.find.agent.activity.AgentQuestionsActivity.LAST_NAME;
import static com.find.agent.activity.CustomerRegistrationNameActivity.CUSTOMER_REGISTRATION_VIA_FACEBOOK;
import static com.find.agent.activity.LoginActivity.FACEBOOK_LOGIN_AND_REGISTRATION;

public class CustomerMapsActivity extends BaseActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    ProgressLoadingLocationDialog progressLoadingLocationDialog;

    private String[] navMenuTitles;

    private final long TIME_TO_WAKEUP_SERVICE = 2000;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public static String FIRST_NAME = "firstName";
    public static String LAST_NAME = "LastName";
    public static String EMAIL = "email";
    public static String PHONE = "bestPhoneNo";
    public static String PASSWORD = "pass";

    public static final String CUSTOMER_PREFERENCES = "CustomerPrefs";
    public static final String CUSTOMER_PREFERENCES_FACEBOOK = "CustomerPrefsFB";
    public static final String LoginCustomer = "Login";
    public static final String LastNameCustomer = "LastName";
    public static final String FirstNameCustomer = "firstName";
    public static final String NameCustomer = "name";
    public static final String EmailCustomer = "email";
    public static final String PhoneCustomer = "bestPhoneNo";
    public static final String ObjectId = "objectId";

    public static final String agent = "agent";
    public static final String latitude = "latitude";
    public static final String longitude = "longitude";
    SharedPreferences sharedPreferencesCustomer;
    SharedPreferences.Editor editor;

    String firstName;
    String lastName;
    String email;
    String phone = "";
    BackendlessUser backendlessUser;

    String password = "1234567";

    private String objectIdCustomer;
    private AddAgentToMap addAgentToMap;
    private Intent serviceIntent;
    private PendingIntent pendingIntent;
    private AlarmManager alarmManager;
    private static final int REQUEST_CODE_CITY_ACTIVITY = 1000;
    public static Boolean AGENT_TYPE_ON_RC = false;
    public static Boolean AGENT_TYPE_ON_RR = false;
    public static Boolean AGENT_TYPE_ON_Ar = false; //rental
    public static Boolean AGENT_TYPE_ON_RCR = false;

    RelativeLayout layoutRR, layoutRC, layoutRCR, layoutAr;
    ImageView ivRR, ivRC, ivRCR, ivAr;
    ImageView info_ar, info_ac, info_acr, info_acrr;
    public static String selectedLanguge = "";


    public void showDialogOnClick(){


        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.language_choose_dialog);


        dialog.findViewById(R.id.tvCLose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        // set the custom dialog components - text, image and button
        dialog.findViewById(R.id.ic_english).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                selectedLanguge = getString(R.string.txt_english);
                restartAgain(R.drawable.ic_english);
            }
        });

        // set the custom dialog components - text, image and button
        dialog.findViewById(R.id.ic_french).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                selectedLanguge = getString(R.string.txt_french);
                restartAgain(R.drawable.ic_french);
            }
        });

        // set the custom dialog components - text, image and button
        dialog.findViewById(R.id.ic_portuguese).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                selectedLanguge = getString(R.string.txt_portuguese);
                restartAgain(R.drawable.ic_portuguese);
            }
        });

        // set the custom dialog components - text, image and button
        dialog.findViewById(R.id.ic_spanish).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                selectedLanguge = getString(R.string.txt_spanish);
                restartAgain(R.drawable.ic_spanish);
            }
        });

        // set the custom dialog components - text, image and button
        dialog.findViewById(R.id.ic_russian).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                selectedLanguge = getString(R.string.txt_russian);
                restartAgain(R.drawable.ic_russian);
            }
        });

        // set the custom dialog components - text, image and button
        dialog.findViewById(R.id.ic_italian).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                selectedLanguge = getString(R.string.txt_italian);
                restartAgain(R.drawable.ic_italian);
            }
        });


        dialog.show();
    }

    private void restartAgain(int drawable) {
        mLangaugeChooseiv.setImageResource(drawable);
        onPause();
        onPostResume();
    }

    ImageView mLangaugeChooseiv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_maps);

        selectedLanguge = getString(R.string.txt_english);
        checkLocationPermission();
        mLangaugeChooseiv = (ImageView)findViewById(R.id.ic_language_select);
        mLangaugeChooseiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogOnClick();
            }
        });

        findViewById(R.id.ic_city).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cityIntent = new Intent(CustomerMapsActivity.this, CityActivity.class);
                startActivityForResult(cityIntent, REQUEST_CODE_CITY_ACTIVITY);
            }
        });

        findViewById(R.id.my_loc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double lat = myLatitude;
                double lng = myLongitude;
                LatLng latLng = new LatLng(lat, lng);
                moveToLatLng(latLng, getString(R.string.my_location));
            }
        });
        initView();
    }
    double myLatitude, myLongitude;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE_CITY_ACTIVITY && resultCode == RESULT_OK){
            if(data != null && data.hasExtra("lat")) {
                String lat = data.getStringExtra("lat");
                String lng = data.getStringExtra("lng");
                LatLng latLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
                moveToLatLng(latLng, data.getStringExtra("name"));
            }
        }
    }

    private void moveToLatLng(LatLng latLng, String name) {

        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        updateLocationOnBackendless(latLng.latitude, latLng.latitude);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        MarkerOptions markerOptions = new MarkerOptions().position(latLng)
                .title(name);
        mCurrLocationMarker = mMap.addMarker(markerOptions);
        addAgentToMap.setMyLocation(latLng);
    }

    public void setDefault(){
        layoutRR.setSelected(false);
        layoutRC.setSelected(false);
        layoutRCR.setSelected(false);
        layoutAr.setSelected(false);
        ivRR.setVisibility(View.GONE);
        ivRCR.setVisibility(View.GONE);
        ivRC.setVisibility(View.GONE);
        ivAr.setVisibility(View.GONE);

        AGENT_TYPE_ON_Ar = false;
        AGENT_TYPE_ON_RR = false;
        AGENT_TYPE_ON_RCR = false;
        AGENT_TYPE_ON_RC = false;
    }

    public void setSelection(int value, boolean isChecked){
        setDefault();

        switch (value){
            case 1:
                layoutRR.setSelected(isChecked ? true : false);
                ivRR.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                AGENT_TYPE_ON_RR = isChecked;
                break;
            case 2:
                layoutRC.setSelected(isChecked ? true : false);
                ivRC.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                AGENT_TYPE_ON_RC = isChecked;
                break;
            case 3:
                layoutRCR.setSelected(isChecked ? true : false);
                ivRCR.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                AGENT_TYPE_ON_RCR = isChecked;
                break;
            case 4:
                layoutAr.setSelected(isChecked ? true : false);
                ivAr.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                AGENT_TYPE_ON_Ar = isChecked;
                break;
        }


        onPause();
        onPostResume();


    }

    private void initView() {
        layoutRR = (RelativeLayout) findViewById(R.id.layout_rr);
        layoutRC = (RelativeLayout) findViewById(R.id.layout_rc);
        layoutAr = (RelativeLayout) findViewById(R.id.layout_ar);
        layoutRCR = (RelativeLayout) findViewById(R.id.layout_rcr);
        ivRR = (ImageView) findViewById(R.id.iv_rr);
        ivRC = (ImageView) findViewById(R.id.iv_rc);
        ivAr = (ImageView) findViewById(R.id.iv_ar);
        ivRCR = (ImageView) findViewById(R.id.iv_rcr);

        info_ac=  (ImageView) findViewById(R.id.info_ac);
        info_acr = (ImageView) findViewById(R.id.info_acr);
        info_ar = (ImageView) findViewById(R.id.info_ar);
        info_acrr = (ImageView) findViewById(R.id.info_acrr);

        info_ac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToolTop(getString(R.string.str_ac_full), v);
            }
        });

        info_ar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToolTop(getString(R.string.str_ar_full), v);
            }
        });

        info_acr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToolTop(getString(R.string.str_ar_full), v);
            }
        });

        info_acrr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToolTop(getString(R.string.str_rcr_full), v);
            }
        });

        //default selection
        setDefault();
        setSelection(3, true);

        layoutRR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection(1, !(ivRR.getVisibility() == View.VISIBLE));
            }
        });

        layoutAr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection(4, !(ivAr.getVisibility() == View.VISIBLE));
            }
        });


        layoutRC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection(2, !(ivRC.getVisibility() == View.VISIBLE));
            }
        });

        layoutRCR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection(3, !(ivRCR.getVisibility() == View.VISIBLE));
            }
        });


        progressLoadingLocationDialog = new ProgressLoadingLocationDialog(this);
        progressLoadingLocationDialog.setCancelable(false);
        progressLoadingLocationDialog.show();

        if (!FACEBOOK_LOGIN_AND_REGISTRATION)
            sharedPreferencesCustomer = getSharedPreferences(CUSTOMER_PREFERENCES, Context.MODE_PRIVATE);
        else
            sharedPreferencesCustomer = getSharedPreferences(CUSTOMER_PREFERENCES_FACEBOOK, Context.MODE_PRIVATE);

        editor = sharedPreferencesCustomer.edit();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        setUpActionBarEndTextStyle();

        getExtras();
    }

    private void showToolTop(String str_ac_full, View view) {
        Tooltip.make(this,
                new Tooltip.Builder(101)
                        .anchor(view, Tooltip.Gravity.BOTTOM)
                        .closePolicy(new Tooltip.ClosePolicy()
                                .insidePolicy(true, false)
                                .outsidePolicy(true, false), 3000)
                        .activateDelay(100)
                        .showDelay(100)
                        .text(str_ac_full)
                        .maxWidth(500)
                        .withArrow(true)
                        .withOverlay(true)
                        //.typeface(mYourCustomFont)
                        //.floatingAnimation(Tooltip.AnimationBuilder.DEFAULT)
                        .build()
        ).show();
    }

    private void setUpActionBarEndTextStyle() {
        SpannableString s = new SpannableString("Maps");
        s.setSpan(new TypefaceSpan(this, "Lato-Bold.ttf"), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        // Update the action bar title with the TypefaceSpan instance
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(s);

        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items); // load titles from strings.xml
        set(navMenuTitles);
    }

    private void getExtras() {
        firstName = getIntent().getStringExtra(FIRST_NAME);
        lastName = getIntent().getStringExtra(LAST_NAME);
        email = getIntent().getStringExtra(EMAIL);
        phone = getIntent().getStringExtra(PHONE);
        password = getIntent().getStringExtra(PASSWORD);

        objectIdCustomer = sharedPreferencesCustomer.getString(ObjectId, "null");
        loginInBackendless(0, 0);
    }

    private void loginInBackendless(final double lati, final double longi) {
        Backendless.UserService.login(firstName, password, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser response) {
                progressLoadingLocationDialog.dismiss();

                if(response.getObjectId() !=null) {
                    backendlessUser = response;
                    String name = (response.getProperty(FIRST_NAME) != null ? response.getProperty(FIRST_NAME).toString() + " " : "") + (response.getProperty(LAST_NAME) != null ? response.getProperty(LAST_NAME).toString() : "");
                    firstName = response.getProperty(FirstNameCustomer).toString();
                    lastName = response.getProperty(LastNameCustomer).toString();
                    email = response.getEmail();
                    phone = response.getProperty(PhoneCustomer).toString();

                    editor.putBoolean(LoginCustomer, true);
                    editor.putString(FirstNameCustomer, firstName);
                    editor.putString(LastNameCustomer, lastName);
                    editor.putString(NameCustomer, name);
                    editor.putString(EmailCustomer, email);
                    editor.putString(PhoneCustomer, phone);
                    editor.putString(ObjectId, response.getObjectId());
                    editor.commit();
                    if (addAgentToMap == null) {
                        createServiceWhoAddAgent();
                    }

                    setIntentData(response.getObjectId(), true);
                }
//                else{
//                    saveOndBackendless(lati, longi);
//                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                if (phone.equals(CUSTOMER_REGISTRATION_VIA_FACEBOOK)) {
                    AlertDialog.Builder dialogPhoneNumber = new AlertDialog.Builder(CustomerMapsActivity.this);

                    View v = getLayoutInflater().inflate(R.layout.phone_for_dialog_btn, null);
                    final EditText editText = (EditText) v.findViewById(R.id.editText);

                    dialogPhoneNumber.setView(v);
                    dialogPhoneNumber.setTitle("Please enter you phone number");
                    dialogPhoneNumber.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            phone = editText.getText().toString();
                            saveOndBackendless(lati, longi);
                        }
                    });
                    dialogPhoneNumber.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    });

                    dialogPhoneNumber.show();
                } else {
                    saveOndBackendless(lati, longi); //if we have en error then possible we dont register yet.
                }
            }
        });
    }

    public void saveOndBackendless(final Double lati, final Double longi) {
        BackendlessUser user = new BackendlessUser();
        user.setProperty(FirstNameCustomer, firstName);
        user.setProperty(LastNameCustomer, lastName);
        user.setProperty(NameCustomer, firstName);
        user.setProperty(PhoneCustomer, phone);
        user.setProperty("latitude", String.valueOf(lati));
        user.setProperty("longitude", String.valueOf(longi));
        user.setProperty(LoginActivity.Customer, false);
        user.setEmail(email);
        user.setPassword(password);


        Backendless.UserService.register(user, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser response) {
                progressLoadingLocationDialog.dismiss();

                editor.putBoolean(LoginCustomer, true);
                editor.putString(FirstNameCustomer, firstName);
                editor.putString(LastNameCustomer, lastName);
                editor.putString(NameCustomer, firstName);
                editor.putString(EmailCustomer, email);
                editor.putString(PhoneCustomer, phone);
                editor.putString(ObjectId, response.getObjectId());
                editor.commit();

                if (addAgentToMap == null) {
                    createServiceWhoAddAgent();
                }

                setIntentData(response.getObjectId(), true);
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                progressLoadingLocationDialog.dismiss();
                if (fault.getCode().equals("Internal client exception")) {
                    Toast.makeText(CustomerMapsActivity.this, "Check your internet connection", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(CustomerMapsActivity.this, "Backendless Eroor:   " + fault.getMessage(), Toast.LENGTH_LONG).show();
                }

                Log.e("Backendless Eroor", fault.getMessage() + "   ::  " + fault.getCode());
            }
        });
    }


    private void updateLocationOnBackendless(final double lati, final double longi) {
        Backendless.UserService.login(firstName, password, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser response) {
                response.setProperty("latitude", String.valueOf(lati));
                response.setProperty("longitude", String.valueOf(longi));



                Backendless.UserService.update(response, new AsyncCallback<BackendlessUser>() {
                    @Override
                    public void handleResponse(BackendlessUser backendlessUser) {

                    }

                    @Override
                    public void handleFault(BackendlessFault backendlessFault) {

                    }
                });
                if (addAgentToMap == null) {
                    createServiceWhoAddAgent();
                }
            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {

            }
        });
    }

    private void createServiceWhoAddAgent() {
        stopService(getIntent());
        serviceIntent = new Intent(this, AddAgentToMap.class);
        bindService(serviceIntent, mServerConn, BIND_AUTO_CREATE);
        pendingIntent = PendingIntent.getService(this, 0, serviceIntent, 0);
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, TIME_TO_WAKEUP_SERVICE, pendingIntent);
    }

    protected ServiceConnection mServerConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            AddAgentToMap.LocalBinder mLocalBinder = (AddAgentToMap.LocalBinder) binder;
            addAgentToMap = mLocalBinder.getServerInstance();
            addAgentToMap.setGoogleMap(mMap);
            addAgentToMap.setMyLocation(mLastLocation);
            Log.d("Log", "onServiceConnected");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            addAgentToMap = null;
            Log.d("Log", "onServiceDisconnected");
        }
    };

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
//                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
//            mMap.setMyLocationEnabled(true);

        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        progressLoadingLocationDialog.dismiss();
        Toast.makeText(this, "Error " + connectionResult.getErrorMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLocationChanged(Location location) {
        onLocationCh(location);
    }

    private void onLocationCh(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        myLatitude = location.getLatitude();
        myLongitude = location.getLongitude();
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        updateLocationOnBackendless(location.getLatitude(), location.getLongitude());

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(CustomerMapsActivity.this);
        builder.setTitle("Log out?");
        builder.setMessage("Are you sure, you want to Logout?");
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                CustomerMapsActivity.this.finish();
                unbindService();
            }
        }).show();
    }

    private void unbindService() {
        if ((serviceIntent != null) && (mServerConn != null)) {
            alarmManager.cancel(pendingIntent);
            unbindService(mServerConn);
        }
    }

    @Override
    protected void onPause() {
        if ((serviceIntent != null) && (alarmManager != null)) {
            alarmManager.cancel(pendingIntent);
        }


        super.onPause();
    }

    @Override
    protected void onPostResume() {
        if ((serviceIntent != null) && (alarmManager != null)) {
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, 1, TIME_TO_WAKEUP_SERVICE, pendingIntent);
        }
        super.onPostResume();
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean setVisabylityCustomerTypeIcon() {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.agentType) {
            AlertDialog.Builder myDialogTypeAgent = new AlertDialog.Builder(this);
            View v = View.inflate(this, R.layout.custom_dialog_customer, null);
            final CheckBox redAgentCheckBox = (CheckBox) v.findViewById(R.id.redAgentCheckBox);
            final CheckBox commAgentCheckBox = (CheckBox) v.findViewById(R.id.commAgentCheckBox);
            final CheckBox bothAgentCheckBox = (CheckBox) v.findViewById(R.id.bothAgentCheckBox);

            myDialogTypeAgent.setView(v);
            myDialogTypeAgent.setTitle("Choose Agent type.");
            myDialogTypeAgent.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (redAgentCheckBox.isChecked()) AGENT_TYPE_ON_RR = true;
                    else AGENT_TYPE_ON_RR = false;
                    if (commAgentCheckBox.isChecked()) AGENT_TYPE_ON_RC = true;
                    else AGENT_TYPE_ON_RC = false;
                    if (bothAgentCheckBox.isChecked()) AGENT_TYPE_ON_RCR = true;
                    else AGENT_TYPE_ON_RCR = false;
                    onPause();
                    onPostResume();
                }
            });
            myDialogTypeAgent.setNegativeButton("CANCEL", null);
            myDialogTypeAgent.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
//                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                }
                return;
            }

        }
    }





}
