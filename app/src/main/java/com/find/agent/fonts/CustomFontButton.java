package com.find.agent.fonts;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.ProgressBar;

/**
 * Created by norman on 3/8/15.
 */
public class CustomFontButton extends Button {

    public CustomFontButton(Context context) {
        super(context);
        if(!this.isInEditMode()) {
            CustomFontUtils.applyCustomFont(this, context, null);
        }
    }

    public CustomFontButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(!this.isInEditMode()) {
            CustomFontUtils.applyCustomFont(this, context, attrs);
        }
    }

    public CustomFontButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if(!this.isInEditMode()) {
            CustomFontUtils.applyCustomFont(this, context, attrs);
        }
    }
}