package com.find.agent.fonts;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.find.agent.R;


/**
 * Created by norman on 3/8/15.
 */
public class CustomFontUtils {

    public static final String ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android";

    public static void applyCustomFont(TextView customFontTextView, Context context, AttributeSet attrs) {
        TypedArray attributeArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.CustomFontTextView);

        String fontName = attributeArray.getString(R.styleable.CustomFontTextView_font);

        // check if a special textStyle was used (e.g. extra bold)
        int textStyle = attributeArray.getInt(R.styleable.CustomFontTextView_textStyle, 0);

        // if nothing extra was used, fall back to regular android:textStyle parameter
        if (textStyle == 0) {
            textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);
        }

        Typeface customFont = selectTypeface(context, fontName, textStyle);
        customFontTextView.setTypeface(customFont);

        attributeArray.recycle();
    }

    private static Typeface selectTypeface(Context context, String fontName, int textStyle) {

        if (fontName.contentEquals(context.getString(R.string.font_name_fontawesome))) {
            return FontCache.getTypeface("fonts/fontawesome.ttf", context);
        }
        else if (fontName.contentEquals(context.getString(R.string.font_name_lato))) {
            /*
            information about the TextView textStyle:
            http://developer.android.com/reference/android/R.styleable.html#TextView_textStyle
            */
            switch (textStyle) {
                case Typeface.BOLD: // bold
                    return FontCache.getTypeface("fonts/Lato-Bold.ttf", context);

                case Typeface.ITALIC: // italic
                    return FontCache.getTypeface("fonts/Lato-Italic.ttf", context);

                case 9: // bold italic
                    return FontCache.getTypeface("fonts/Lato-BoldItalic.ttf", context);

                case 10: // extra light, equals @integer/font_style_black_10
                    return FontCache.getTypeface("fonts/Lato-Black.ttf", context);

                case 11: // extra bold, equals @integer/font_style_black_italic_11
                    return FontCache.getTypeface("fonts/Lato-BlackItalic.ttf", context);

                case 12: // extra bold, equals @integer/font_style_hairline_12
                    return FontCache.getTypeface("fonts/Lato-Hairline.ttf", context);

                case 13: // extra bold, equals @integer/font_style_hairline_italic_13
                    return FontCache.getTypeface("fonts/Lato-HairlineItalic.ttf", context);

                case 14: // extra bold, equals @integer/font_style_heavy_14
                    return FontCache.getTypeface("fonts/Lato-Heavy.ttf", context);

                case 15: // extra bold, equals @integer/font_style_heavy_italic_15
                    return FontCache.getTypeface("fonts/Lato-HeavyItalic.ttf", context);

                case 16: // extra bold, equals @integer/font_style_light_16
                    return FontCache.getTypeface("fonts/Lato-Light.ttf", context);

                case 17: // extra bold, equals @integer/font_style_light_italic_17
                    return FontCache.getTypeface("fonts/Lato-LightItalic.ttf", context);

                case 18: // extra bold, equals @integer/font_style_medium_18
                    return FontCache.getTypeface("fonts/Lato-Medium.ttf", context);

                case 19: // extra bold, equals @integer/font_style_medium_ialic_19
                    return FontCache.getTypeface("fonts/Lato-MediumItalic.ttf", context);

                case 20: // extra bold, equals @integer/font_style_semibold_20
                    return FontCache.getTypeface("fonts/Lato-Semibold.ttf", context);

                case 21: // extra bold, equals @integer/font_style_semibold_italic_21
                    return FontCache.getTypeface("fonts/Lato-SemiboldItalic.ttf", context);

                case 22: // extra bold, equals @integer/font_style_thin_22
                    return FontCache.getTypeface("fonts/Lato-Thin.ttf", context);

                case 23: // extra bold, equals @integer/font_style_thin_italic_23
                    return FontCache.getTypeface("fonts/Lato-ThinItalic.ttf", context);

                case Typeface.NORMAL: // regular
                default:
                    return FontCache.getTypeface("fonts/Lato-Regular.ttf", context);
            }
        }

        else if (fontName.contentEquals(context.getString(R.string.font_name_raleway))) {
            /*
            information about the TextView textStyle:
            http://developer.android.com/reference/android/R.styleable.html#TextView_textStyle
            */
            switch (textStyle) {
                case Typeface.BOLD: // bold
                    return FontCache.getTypeface("fonts/Raleway-Bold.ttf", context);

                case 10: // extra light, equals @integer/font_style_black_10
                    return FontCache.getTypeface("fonts/Raleway-ExtraLight.ttf", context);

                case 11: // extra bold, equals @integer/font_style_black_italic_11
                    return FontCache.getTypeface("fonts/Raleway-ExtraBold.ttf", context);


                case 14: // extra bold, equals @integer/font_style_heavy_14
                    return FontCache.getTypeface("fonts/Raleway-Heavy.ttf", context);


                case 16: // extra bold, equals @integer/font_style_light_16
                    return FontCache.getTypeface("fonts/Raleway-Light.ttf", context);


                case 18: // extra bold, equals @integer/font_style_medium_18
                    return FontCache.getTypeface("fonts/Raleway-Medium.ttf", context);


                case 20: // extra bold, equals @integer/font_style_semibold_20
                    return FontCache.getTypeface("fonts/Raleway-SemiBold.ttf", context);


                case 22: // extra bold, equals @integer/font_style_thin_22
                    return FontCache.getTypeface("fonts/Raleway-Thin.ttf", context);

                case Typeface.NORMAL: // regular
                default:
                    return FontCache.getTypeface("fonts/Raleway-Regular.ttf", context);
            }
        }

        else {
            // no matching font found
            // return null so Android just uses the standard font (Roboto)
            return null;
        }
    }
}