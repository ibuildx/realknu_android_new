package com.find.agent.fonts;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;

/**
 * Created by norman on 3/8/15.
 */
public class CustomFontCheckBox extends CheckBox {

    public CustomFontCheckBox(Context context) {
        super(context);
        if(!this.isInEditMode()) {
            CustomFontUtils.applyCustomFont(this, context, null);
        }
    }

    public CustomFontCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(!this.isInEditMode()) {
            CustomFontUtils.applyCustomFont(this, context, attrs);
        }
    }

    public CustomFontCheckBox(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if(!this.isInEditMode()) {
            CustomFontUtils.applyCustomFont(this, context, attrs);
        }
    }
}